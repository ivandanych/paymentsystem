package com.epam.training.danychivan.exception;

public class GeneratePdfException extends Exception {

    public GeneratePdfException() {
        super();
    }

    public GeneratePdfException(String message) {
        super(message);
    }

    public GeneratePdfException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeneratePdfException(Throwable cause) {
        super(cause);
    }

    protected GeneratePdfException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
