package com.epam.training.danychivan.constants;

public class PathConstants {

    public static final String REGISTRATION_PATH = "/registration";
    public static final String LOGIN_PATH = "/login";
    public static final String HOME_PATH = "/home";
    public static final String PROFILE_PATH = "/profile";
    public static final String CUSTOMER_ACCOUNTS_PATH = "/accounts";
    public static final String CUSTOMER_PAYMENTS_PATH = "/payments";
    public static final String CUSTOMER_RECEIPTS_PATH = "/receipts";
    public static final String CUSTOMER_WITHDRAWS_PATH = "/withdraws";
    public static final String ADMIN_CUSTOMERS_PATH = "/admin/customers";
    public static final String ADMIN_ACCOUNTS_PATH = "/admin/accounts";
    public static final String ADMIN_UNBLOCK_REQUEST_PATH = "/admin/unblock-requests";
    public static final String CREATE_ACCOUNT = "/accounts/create";
    public static final String CREATE_PAYMENT = "/payments/create";
    public static final String CREATE_RECEIPT = "/receipts/create";
    public static final String CREATE_WITHDRAW = "/withdraws/create";
    public static final String PERMISSION_ERROR = "/permission-error";
    public static final String ERROR_PAGE_PATH = "/error-page";
    public static final String INTERNAL_ERROR = "/internal-error";
}
