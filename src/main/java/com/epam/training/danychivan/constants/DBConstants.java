package com.epam.training.danychivan.constants;

public class DBConstants {

    public static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/paypool?user=root&password=12345678&serverTimezone=UTC";

    // Table names
    public static final String CUSTOMER_TABLE_NAME = "customer";

    public static final String ACCOUNT_TABLE_NAME = "account";

    public static final String PAYMENT_TABLE_NAME = "payment";

    public static final String RECEIPT_TABLE_NAME = "receipt";

    public static final String WITHDRAW_TABLE_NAME = "withdraw";

    public static final String UNBLOCK_REQUEST_TABLE_NAME = "unblock_request";

    public static final String STATUS_DETAILS_TABLE_NAME = "status_details";


    // Customer queries
    public static final String GET_ALL_CUSTOMERS = "SELECT " +
            "id, login, password, email, name, surname, role_id, is_blocked, create_date, last_update " +
            "FROM " + CUSTOMER_TABLE_NAME;

    public static final String GET_CUSTOMER_BY_ID = "SELECT " +
            "id, login, password, email, name, surname, role_id, is_blocked, create_date, last_update " +
            "FROM " + CUSTOMER_TABLE_NAME +
            " WHERE id = ?";

    public static final String GET_CUSTOMER_BY_LOGIN = "SELECT " +
            "id, login, password, email, name, surname, role_id, is_blocked, create_date, last_update " +
            "FROM " + CUSTOMER_TABLE_NAME +
            " WHERE login = ?";

    public static final String GET_CUSTOMER_BY_EMAIL = "SELECT" +
            " id, login, password, email, name, surname, role_id, is_blocked, create_date, last_update" +
            " FROM " + CUSTOMER_TABLE_NAME +
            " WHERE email = ?";

    public static final String GET_CUSTOMERS_OFFSET_LIMIT_ORDER_BY_ASC = "SELECT" +
            " id, login, password, email, name, surname, role_id, is_blocked, create_date, last_update" +
            " FROM " + CUSTOMER_TABLE_NAME +
            " ORDER BY ?" +
            " LIMIT ?, ?";

    public static final String GET_CUSTOMERS_OFFSET_LIMIT_ORDER_BY_DESC = "SELECT" +
            " id, login, password, email, name, surname, role_id, is_blocked, create_date, last_update" +
            " FROM " + CUSTOMER_TABLE_NAME +
            " ORDER BY ? DESC" +
            " LIMIT ?, ?";

    public static final String COUNT_CUSTOMERS = "SELECT COUNT(1)" +
            " FROM " + CUSTOMER_TABLE_NAME;

    public static final String INSERT_CUSTOMER = "INSERT INTO " + CUSTOMER_TABLE_NAME +
            "(login, password, email, name, surname)" +
            "VALUES(?, ?, ?, ?, ?)";

    public static final String DELETE_CUSTOMER_BY_LOGIN = "DELETE FROM " + CUSTOMER_TABLE_NAME +
            " WHERE login = ?";

    public static final String BLOCK_UNBLOCK_CUSTOMER_BY_ID = "UPDATE " + CUSTOMER_TABLE_NAME +
            " SET is_blocked = NOT is_blocked" +
            " WHERE id = ?";

    // Account queries

    public static final String GET_ALL_ACCOUNTS = "SELECT" +
            " id, name, balance, credit_card_number, is_blocked, customer_id, create_date, last_update" +
            " FROM " + ACCOUNT_TABLE_NAME;

    public static final String GET_ACCOUNT_BY_ID = "SELECT" +
            " id, name, balance, credit_card_number, is_blocked, customer_id, create_date, last_update" +
            " FROM " + ACCOUNT_TABLE_NAME +
            " WHERE id = ?";

    public static final String GET_ACCOUNT_BY_CREDIT_CARD = "SELECT" +
            " id, name, balance, credit_card_number, is_blocked, customer_id, create_date, last_update" +
            " FROM " + ACCOUNT_TABLE_NAME +
            " WHERE credit_card_number = ?";

    public static final String GET_ALL_ACCOUNTS_BY_CUSTOMER_ID = "SELECT" +
            " id, name, balance, credit_card_number, is_blocked, customer_id, create_date, last_update" +
            " FROM " + ACCOUNT_TABLE_NAME +
            " WHERE customer_id = ?";

    public static final String GET_NOT_BLOCKED_ACCOUNTS_BY_CUSTOMER_ID = "SELECT" +
            " id, name, balance, credit_card_number, is_blocked, customer_id, create_date, last_update" +
            " FROM " + ACCOUNT_TABLE_NAME +
            " WHERE customer_id = ? AND is_blocked = 0";

    public static final String GET_ACCOUNTS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_ASC = "SELECT" +
            " id, name, balance, credit_card_number, is_blocked, customer_id, create_date, last_update" +
            " FROM " + ACCOUNT_TABLE_NAME +
            " WHERE customer_id = ?" +
            " ORDER BY ?" +
            " LIMIT ?, ?";

    public static final String GET_ACCOUNTS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_DESC = "SELECT" +
            " id, name, balance, credit_card_number, is_blocked, customer_id, create_date, last_update" +
            " FROM " + ACCOUNT_TABLE_NAME +
            " WHERE customer_id = ?" +
            " ORDER BY ? DESC" +
            " LIMIT ?, ?";

    public static final String GET_ACCOUNTS_OFFSET_LIMIT_ORDER_BY_ASC = "SELECT" +
            " id, name, balance, credit_card_number, is_blocked, customer_id, create_date, last_update" +
            " FROM " + ACCOUNT_TABLE_NAME +
            " ORDER BY ?" +
            " LIMIT ?, ?";

    public static final String GET_ACCOUNTS_OFFSET_LIMIT_ORDER_BY_DESC = "SELECT" +
            " id, name, balance, credit_card_number, is_blocked, customer_id, create_date, last_update" +
            " FROM " + ACCOUNT_TABLE_NAME +
            " ORDER BY ? DESC" +
            " LIMIT ?, ?";

    public static final String COUNT_ACCOUNTS = "SELECT COUNT(1)" +
            " FROM " + ACCOUNT_TABLE_NAME;

    public static final String COUNT_ACCOUNT_BY_CUSTOMER_ID = "SELECT COUNT(1)" +
            " FROM " + ACCOUNT_TABLE_NAME +
            " WHERE customer_id = ?";

    public static final String INSERT_ACCOUNT = "INSERT INTO " + ACCOUNT_TABLE_NAME +
            " (name, credit_card_number, customer_id)" +
            " VALUES (?, ?, ?)";

    public static final String UPDATE_BALANCE_ACCOUNT_BY_ID = "UPDATE " + ACCOUNT_TABLE_NAME +
            " SET balance = ?" +
            " WHERE id = ?";

    public static final String DELETE_ACCOUNT_BY_ID = "DELETE FROM " + ACCOUNT_TABLE_NAME +
            " WHERE id = ?";

    public static final String BLOCK_UNBLOCK_ACCOUNT_BY_ID = "UPDATE " + ACCOUNT_TABLE_NAME +
            " SET is_blocked = NOT is_blocked" +
            " WHERE id = ?";

    public static final String GET_ALL_ACCOUNTS_ID_BY_CUSTOMER_ID = "SELECT id" +
            " FROM " + ACCOUNT_TABLE_NAME +
            " WHERE customer_id = ?";

    // Payment queries

    public static final String GET_ALL_PAYMENTS_BY_CUSTOMER_ID = "SELECT" +
            " id, from_account_id, to_account_id, value, create_date, last_update, status_id" +
            " FROM " + PAYMENT_TABLE_NAME +
            " WHERE from_account_id IN (" + GET_ALL_ACCOUNTS_ID_BY_CUSTOMER_ID +  ")" +
            " OR to_account_id IN (" + GET_ALL_ACCOUNTS_ID_BY_CUSTOMER_ID +  ")";

    public static final String GET_ALL_PAYMENTS_BY_ACCOUNT_ID = "SELECT" +
            " id, from_account_id, to_account_id, value, create_date, last_update, status_id" +
            " FROM " + PAYMENT_TABLE_NAME +
            " WHERE from_account_id = ? OR to_account_id = ?";

    public static final String GET_PAYMENT_BY_ID = "SELECT" +
            " id, from_account_id, to_account_id, value, create_date, last_update, status_id" +
            " FROM " + PAYMENT_TABLE_NAME +
            " WHERE id = ?";

    public static final String GET_PAYMENTS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_ASC = "SELECT" +
            " id, from_account_id, to_account_id, value, create_date, last_update, status_id" +
            " FROM " + PAYMENT_TABLE_NAME +
            " WHERE from_account_id IN (SELECT id FROM account WHERE customer_id = ?) OR" +
            " to_account_id IN (SELECT id FROM account WHERE customer_id = ?)" +
            " ORDER BY ?" +
            " LIMIT ?, ?";

    public static final String GET_PAYMENTS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_DESC = "SELECT" +
            " id, from_account_id, to_account_id, value, create_date, last_update, status_id" +
            " FROM " + PAYMENT_TABLE_NAME +
            " WHERE from_account_id IN (SELECT id FROM account WHERE customer_id = ?) OR" +
            " to_account_id IN (SELECT id FROM account WHERE customer_id = ?)" +
            " ORDER BY ? DESC" +
            " LIMIT ?, ?";

    public static final String COUNT_PAYMENTS_BY_CUSTOMER_ID = "SELECT COUNT(1)" +
            " FROM " + PAYMENT_TABLE_NAME +
            " WHERE from_account_id IN (SELECT id FROM account WHERE customer_id = ?) OR" +
            " to_account_id IN (SELECT id FROM account WHERE customer_id = ?)";

    public static final String INSERT_PAYMENT = "INSERT INTO " + PAYMENT_TABLE_NAME +
            " (from_account_id, to_account_id, value, status_id)" +
            " VALUES (?, ?, ?, ?)";

    public static final String DELETE_PAYMENT_BY_ID = "DELETE FROM " + PAYMENT_TABLE_NAME +
            " WHERE id = ?";

    public static final String UPDATE_PAYMENT = "UPDATE payment" +
            " SET value = ?, status_id = ?" +
            " WHERE id = ?";

    // Receipt queries

    public static final String GET_ALL_RECEIPTS_BY_CUSTOMER_ID = "SELECT" +
            " id, to_account_id, value, create_date" +
            " FROM " + RECEIPT_TABLE_NAME +
            " WHERE to_account_id IN (" + GET_ALL_ACCOUNTS_ID_BY_CUSTOMER_ID + ")";

    public static final String GET_ALL_RECEIPTS_BY_ACCOUNT_ID = "SELECT" +
            " id, to_account_id, value, create_date" +
            " FROM " + RECEIPT_TABLE_NAME +
            " WHERE to_account_id = ?";

    public static final String GET_RECEIPTS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_ASC = "SELECT" +
            " id, to_account_id, value, create_date" +
            " FROM " + RECEIPT_TABLE_NAME +
            " WHERE to_account_id IN (SELECT id FROM account WHERE customer_id = ?)" +
            " ORDER BY ?" +
            " LIMIT ?, ?";

    public static final String GET_RECEIPTS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_DESC = "SELECT" +
            " id, to_account_id, value, create_date" +
            " FROM " + RECEIPT_TABLE_NAME +
            " WHERE to_account_id IN (SELECT id FROM account WHERE customer_id = ?)" +
            " ORDER BY ? DESC" +
            " LIMIT ?, ?";

    public static final String COUNT_RECEIPTS_BY_CUSTOMER_ID = "SELECT COUNT(1)" +
            " FROM " + RECEIPT_TABLE_NAME +
            " WHERE to_account_id IN (SELECT id FROM account WHERE customer_id = ?)";

    public static final String INSERT_RECEIPT = "INSERT INTO " + RECEIPT_TABLE_NAME +
            " (to_account_id, value)" +
            " VALUES (?, ?)";

    public static final String DELETE_RECEIPT_BY_ID = "DELETE FROM " + RECEIPT_TABLE_NAME +
            " WHERE id = ?";

    // Withdraw queries

    public static final String GET_ALL_WITHDRAWS_BY_CUSTOMER_ID = "SELECT" +
            " id, from_account_id, value, create_date" +
            " FROM " + WITHDRAW_TABLE_NAME +
            " WHERE from_account_id IN (" + GET_ALL_ACCOUNTS_ID_BY_CUSTOMER_ID + ")";

    public static final String GET_ALL_WITHDRAWS_BY_ACCOUNT_ID = "SELECT" +
            " id, from_account_id, value, create_date" +
            " FROM " + WITHDRAW_TABLE_NAME +
            " WHERE from_account_id = ?";

    public static final String GET_WITHDRAWS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_ASC = "SELECT" +
            " id, from_account_id, value, create_date" +
            " FROM " + WITHDRAW_TABLE_NAME +
            " WHERE from_account_id IN (SELECT id FROM account WHERE customer_id = ?)" +
            " ORDER BY ?" +
            " LIMIT ?, ?";

    public static final String GET_WITHDRAWS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_DESC = "SELECT" +
            " id, from_account_id, value, create_date" +
            " FROM " + WITHDRAW_TABLE_NAME +
            " WHERE from_account_id IN (SELECT id FROM account WHERE customer_id = ?)" +
            " ORDER BY ? DESC" +
            " LIMIT ?, ?";

    public static final String COUNT_WITHDRAWS_BY_CUSTOMER_ID = "SELECT COUNT(1)" +
            " FROM " + WITHDRAW_TABLE_NAME +
            " WHERE from_account_id IN (SELECT id FROM account WHERE customer_id = ?)";

    public static final String INSERT_WITHDRAW = "INSERT INTO " + WITHDRAW_TABLE_NAME +
            " (from_account_id, value)" +
            " VALUES (?, ?)";

    public static final String DELETE_WITHDRAW_BY_ID = "DELETE FROM " + WITHDRAW_TABLE_NAME +
            " WHERE id = ?";


    // UnblockRequest queries

    public static final String GET_UNBLOCK_REQUEST_OFFSET_LIMIT_ORDER_BY_ASC = "SELECT" +
            " id, account_id, is_checked, create_date, last_update" +
            " FROM " + UNBLOCK_REQUEST_TABLE_NAME +
            " ORDER BY ?" +
            " LIMIT ?, ?";

    public static final String GET_UNBLOCK_REQUEST_OFFSET_LIMIT_ORDER_BY_DESC = "SELECT" +
            " id, account_id, is_checked, create_date, last_update" +
            " FROM " + UNBLOCK_REQUEST_TABLE_NAME +
            " ORDER BY ? DESC" +
            " LIMIT ?, ?";

    public static final String INSERT_UNBLOCK_REQUEST = "INSERT INTO " + UNBLOCK_REQUEST_TABLE_NAME +
            " (account_id)" +
            " VALUES (?)";

    public static final String COUNT_UNBLOCK_REQUESTS = "SELECT COUNT(1)" +
            " FROM " + UNBLOCK_REQUEST_TABLE_NAME;

    public static final String COUNT_UNBLOCK_REQUESTS_BY_ACCOUNT_ID = "SELECT COUNT(1)" +
            " FROM " + UNBLOCK_REQUEST_TABLE_NAME +
            " WHERE account_id = ? AND is_checked = 0";

    // Status requests

    public static final String GET_STATUS_DESCRIPTION_BY_ID_AND_LANGUAGE = "SELECT" +
            " description" +
            " FROM " + STATUS_DETAILS_TABLE_NAME +
            " LEFT JOIN language on language_id = language.id" +
            " WHERE status_id = ? AND language.value = ?";

    private DBConstants() {

    }
}
