package com.epam.training.danychivan.util.validation;

import java.util.ArrayList;

public abstract class AbstractValidator<T> implements Validator<T> {

    private final String ERROR_MESSAGE;


    public AbstractValidator(String error_message) {
        ERROR_MESSAGE = error_message;
    }

    @Override
    public String getErrorKey() {
        return ERROR_MESSAGE;
    }

}
