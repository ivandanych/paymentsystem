package com.epam.training.danychivan.util.validation;

public class LengthValidator extends AbstractValidator<String> {
    private final int minLength;
    private final int maxLength;

    public LengthValidator(int minLength,int maxLength, String errorMessage) {
        super(errorMessage);
        this.minLength = minLength;
        this.maxLength = maxLength;
    }

    @Override
    public boolean isValid(String str) {
        return str != null && str.length() >= minLength && str.length() <= maxLength;
    }
}
