package com.epam.training.danychivan.util.validation;

import java.util.regex.Pattern;

public class RegexValidator extends AbstractValidator<String> {

    private final Pattern pattern;

    public RegexValidator(String regex, String errorMessage) {
        super(errorMessage);
        this.pattern = Pattern.compile(regex);
    }

    @Override
    public boolean isValid(String str) {
        if(str == null) {
            return false;
        }
        return pattern.matcher(str).matches();
    }

}
