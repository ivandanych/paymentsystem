package com.epam.training.danychivan.util.validation;

import java.util.ArrayList;
import java.util.List;

public class EmailValidator extends MultiplyValidator<String> {

    private final static String INVALID_EMAIL_LENGTH_KEY = "invalid.email-length";
    private final static int MIN_LENGTH = 4;
    private final static int MAX_LENGTH = 60;

    private final static String INVALID_EMAIL_KEY = "invalid.email";
    private static final String EMAIL_REGEX = "^[A-Za-z0-9_.-]+?@(.+?)$";

    private static List<Validator<String>> validators = new ArrayList<>();

    static {
        validators.add(new LengthValidator(MIN_LENGTH, MAX_LENGTH, INVALID_EMAIL_LENGTH_KEY ));
        validators.add(new RegexValidator(EMAIL_REGEX, INVALID_EMAIL_KEY));
    }

    public EmailValidator() {
        super(validators);
    }
}
