package com.epam.training.danychivan.util.validation;

import java.util.ArrayList;
import java.util.List;

public class LoginValidator extends MultiplyValidator<String> {

    private final static String INVALID_LOGIN_LENGTH_KEY = "invalid.login-length";
    private final static int MIN_LENGTH = 4;
    private final static int MAX_LENGTH = 30;

    private final static String INVALID_LOGIN_KEY = "invalid.login";
    private static final String LOGIN_REGEX = "^[a-z0-9_-]+?$";

    private static List<Validator<String>> validators = new ArrayList<>();

    static {
        validators.add(new LengthValidator(MIN_LENGTH, MAX_LENGTH, INVALID_LOGIN_LENGTH_KEY ));
        validators.add(new RegexValidator(LOGIN_REGEX, INVALID_LOGIN_KEY));
    }

    public LoginValidator() {
        super(validators);
    }

}