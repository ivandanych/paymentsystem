package com.epam.training.danychivan.util.validation;

import java.util.ArrayList;
import java.util.List;

public class NameValidator extends MultiplyValidator<String> {
    private final static String INVALID_NAME_LENGTH_KEY = "invalid.name-length";
    private final static int MIN_LENGTH = 1;
    private final static int MAX_LENGTH = 30;

    private final static String INVALID_NAME_KEY = "invalid.name";
    private static final String NAME_REGEX = "^([a-zA-Z]+)$";

    private static List<Validator<String>> validators = new ArrayList<>();

    static {
        validators.add(new LengthValidator(MIN_LENGTH, MAX_LENGTH, INVALID_NAME_LENGTH_KEY ));
        validators.add(new RegexValidator(NAME_REGEX, INVALID_NAME_KEY));
    }

    public NameValidator() {
        super(validators);
    }
}
