package com.epam.training.danychivan.util.validation;

import java.util.List;

public interface Validator <T> {

    String getErrorKey();

    boolean isValid(T obj);

}
