package com.epam.training.danychivan.util.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class MultiplyValidator<T> {

    private List<String> errors;

    private List<Validator<T>> validators;

    public MultiplyValidator(List<Validator<T>> validators) {
        errors = new ArrayList<>();
        this.validators = validators;
    }

    public List<String> getErrorKeys() {
        return errors;
    }

    public boolean isValid(T obj) {
        errors.clear();
        if (obj == null) {
            return false;
        }
        boolean valid = true;
        for (Validator<T> validator : validators) {
            if(!validator.isValid(obj)) {
                errors.add(validator.getErrorKey());
                valid = false;
            }
        }
        return valid;
    }

    public static <T> List<String> validate(T obj, MultiplyValidator<T> validator, List<String> errors) {
        if(!validator.isValid(obj)) {
            errors.addAll(validator.getErrorKeys());
        }
        return errors;
    }

}
