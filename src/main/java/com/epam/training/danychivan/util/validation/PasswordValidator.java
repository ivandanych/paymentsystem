package com.epam.training.danychivan.util.validation;

import java.util.ArrayList;
import java.util.List;

public class PasswordValidator extends MultiplyValidator<String> {
    private final static String INVALID_PASSWORD_LENGTH_KEY = "invalid.password-length";
    private final static int MIN_LENGTH = 6;
    private final static int MAX_LENGTH = 35;

    private static List<Validator<String>> validators = new ArrayList<>();

    static {
        validators.add(new LengthValidator(MIN_LENGTH, MAX_LENGTH, INVALID_PASSWORD_LENGTH_KEY));
    }

    public PasswordValidator() {
        super(validators);
    }
}
