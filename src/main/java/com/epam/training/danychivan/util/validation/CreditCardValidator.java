package com.epam.training.danychivan.util.validation;

import java.util.ArrayList;
import java.util.List;

public class CreditCardValidator extends MultiplyValidator<String> {

    private final static String INVALID_CREDIT_CARD_KEY = "invalid.credit-card";
    private static final String LOGIN_REGEX = "[0-9]{16}";

    private static List<Validator<String>> validators = new ArrayList<>();

    static {
        validators.add(new RegexValidator(LOGIN_REGEX, INVALID_CREDIT_CARD_KEY));
    }

    public CreditCardValidator() {
        super(validators);
    }

}
