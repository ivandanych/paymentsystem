package com.epam.training.danychivan.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class CeilTag extends TagSupport {

    private double param;

    public void setParam(double param) {
        this.param = param;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            pageContext.getOut().write(Integer.toString((int) Math.ceil(param)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }

}
