package com.epam.training.danychivan.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class FormatCreditCardTag extends TagSupport {

    private String creditCard;

    private char separator;

    public void setSeparator(char separator) {
        this.separator = separator;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    @Override
    public int doStartTag() throws JspException {
        String[] parts = creditCard.split("(?<=\\G[0-9]{4})");
        try {
            pageContext.getOut().write(String.format("%s%c%s%c%s%c%s",
                    parts[0], separator, parts[1], separator, parts[2], separator, parts[3]));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return super.doStartTag();
    }
}
