package com.epam.training.danychivan;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.command.CommandContainer;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.UtilService;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Customer;
import com.epam.training.danychivan.exception.ServerException;
import com.epam.training.danychivan.util.Util;
import javafx.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Front controller, processing all requests
 * @author Ivan Danych
 */

public class ControllerServlet extends HttpServlet {

    private static final Logger logger = LogManager.getLogger(ControllerServlet.class);

    // logging DAO
    // logging target directory

    /**
     * Method which process post requests.
     * If customer is blocked, auto replace command to log out
     @see HttpServlet
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String commandName = request.getParameter("command");
        Command command = CommandContainer.getPostCommand(commandName);
        if (isCustomerBlocked(request)) {
            logger.info("logout for blocked customer");
            command = CommandContainer.getGetCommand("/logout");
        }
        logger.info("Processing post command: " + command +
                " from url: " + request.getRequestURI());
        process(request, response, command);
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Command command = CommandContainer.getGetCommand(request.getRequestURI());
        logger.info("Processing get command: " + command +
                " from url: " + request.getRequestURI());
        process(request, response, command);
    }

    private void process(HttpServletRequest request, HttpServletResponse response, Command command)
            throws ServletException, IOException {
        String result;
        Command.Status status = Command.Status.FORWARD;
        if (command == null) {
            result = request.getRequestURI();
        } else {
            try {
                Pair<Command.Status, String> pair = command.execute(request, response);
                status = pair.getKey();
                result = pair.getValue();
            } catch (DaoException | NullPointerException | ServerException e) {
                logger.error("Some problem while processing command: {}", command, e);
                status = Command.Status.REDIRECT;
                result = PathConstants.INTERNAL_ERROR;
            }
        }

        logger.info("Status: " + status + " result: " + result);
        if(status == Command.Status.FORWARD) {
            result = "/WEB-INF/jsp" + result + ".jsp";
            Util.forward(request, response, result);
        } else if(status == Command.Status.REDIRECT) {
            response.sendRedirect(result);
        }
    }

    private boolean isCustomerBlocked(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer");
        if (customer == null) {
            return false;
        }
        return UtilService.isCustomerBlocked(customer.getId());
    }

}
