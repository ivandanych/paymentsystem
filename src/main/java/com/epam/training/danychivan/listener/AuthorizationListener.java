package com.epam.training.danychivan.listener;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

public class AuthorizationListener implements HttpSessionAttributeListener {

    // Update some resource


    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        if(event.getName().equals("customer")) {
            System.out.println("Login");
        }
    }
}
