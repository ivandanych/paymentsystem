package com.epam.training.danychivan.command.impl.authorization;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.constants.PathConstants;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LogoutCommand extends Command {

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        logger.debug("Customer: {}, was logged out", session.getAttribute("customer"));
        session.removeAttribute("customer");
        return new Pair<>(Status.REDIRECT, PathConstants.HOME_PATH);
    }
}
