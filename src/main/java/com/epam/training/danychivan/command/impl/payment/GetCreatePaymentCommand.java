package com.epam.training.danychivan.command.impl.payment;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.command.GetCreateResourceCommand;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.exception.DaoException;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GetCreatePaymentCommand extends GetCreateResourceCommand {

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        super.execute(request, response);
        return new Pair<>(Status.FORWARD, PathConstants.CREATE_PAYMENT);
    }


}
