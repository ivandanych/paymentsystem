package com.epam.training.danychivan.command.impl.account;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.AccountDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Account;
import com.epam.training.danychivan.util.validation.*;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CreateAccountCommand extends Command {

    private final AccountDAO accountDAO = DAOFactory.getDefaultFactory().getAccountDAO();

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        Account account = extractAccountFromRequest(request);
        List<String> errors = validateData(account);
        boolean notSuccess = false;
        if (!errors.isEmpty() || (notSuccess = !accountDAO.insertAccount(account)) ) {
            logger.info("Errors while customer try to create account: " + errors);
            if (notSuccess) {
                errors.add("account.create.some-problem");
                logger.warn("Some problem with inserting account: " + account);
            }
            request.setAttribute("errors", errors);
            System.out.println(errors);
            return new Pair<>(Status.FORWARD, PathConstants.CREATE_ACCOUNT);
        }
        logger.debug("Account: " + account + " was successfully created");
        return new Pair<>(Status.REDIRECT, PathConstants.CUSTOMER_ACCOUNTS_PATH + "?success=true");
    }

    private List<String> validateData(Account account) throws DaoException {
        List<String> errors = new ArrayList<>();
        MultiplyValidator.validate(account.getCreditCardNumber(), new CreditCardValidator(), errors);

        if (accountDAO.getAccountByCreditCard(account.getCreditCardNumber()) != null) {
            errors.add("account.create.credit-card-exist");
        }
        return errors;
    }

    private Account extractAccountFromRequest(HttpServletRequest request) {
        Account account = new Account();
        account.setName(request.getParameter("name"));
        account.setCreditCardNumber(request.getParameter("creditCard"));
        account.setCustomerId(Integer.parseInt(request.getParameter("customerId")));
        return account;
    }
}
