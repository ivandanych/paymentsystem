package com.epam.training.danychivan.command.impl.account;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.AccountDAO;
import com.epam.training.danychivan.db.dao.UnblockRequestDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Account;
import com.epam.training.danychivan.db.model.Customer;
import com.epam.training.danychivan.db.model.UnblockRequest;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SendUnblockRequestCommand extends Command {

    private final AccountDAO accountDAO = DAOFactory.getDefaultFactory().getAccountDAO();
    private final UnblockRequestDAO unblockRequestDAO = DAOFactory.getDefaultFactory().getUnblockRequestDAO();

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        HttpSession session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer");
        UnblockRequest unblockRequest = extractRequest(request);
        List<String> errors = validateData(unblockRequest, customer);
        boolean notSuccess = false;
        if (!errors.isEmpty() || (notSuccess = !unblockRequestDAO.insertRequest(unblockRequest))) {
            logger.info("Customer with id: {}, can't sent request: {}", customer.getId(), errors);
            if (notSuccess) {
                errors.add("accounts.send-request-some-problem");
                logger.warn("Some problem with insert unblock request!");
            }
            request.setAttribute("errors", errors);
            System.out.println(errors);
            return new Pair<>(Status.FORWARD, PathConstants.ERROR_PAGE_PATH);
        }
        logger.debug("Customer with id: {} sent unblock request for account with id: {}",
                customer.getId(), unblockRequest.getAccountId());
        return new Pair<>(Status.REDIRECT, PathConstants.CUSTOMER_ACCOUNTS_PATH + "?success=true");
    }

    private List<String> validateData(UnblockRequest request, Customer customer) throws DaoException {
        List<String> errors = new ArrayList<>();
        Account account = accountDAO.getAccountById(request.getAccountId());
        if (account == null) {
            errors.add("accounts.exist-error");
            return errors;
        }
        if (account.getCustomerId() != customer.getId()) {
            errors.add("accounts.unblock-request-account-error");
        }
        if (unblockRequestDAO.countNotCheckedRequestByAccountId(account.getId()) != 0) {
            errors.add("accounts.unblock-request-exist-error");
        }
        return errors;
    }

    private UnblockRequest extractRequest(HttpServletRequest request) {
        UnblockRequest unblockRequest = new UnblockRequest();
        unblockRequest.setAccountId(Integer.parseInt(request.getParameter("id")));
        return unblockRequest;
    }
}
