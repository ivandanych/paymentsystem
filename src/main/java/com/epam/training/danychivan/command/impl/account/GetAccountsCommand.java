package com.epam.training.danychivan.command.impl.account;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.command.GetResourceCommand;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.AccountDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetAccountsCommand extends GetResourceCommand {

    private final AccountDAO accountDAO = DAOFactory.getDefaultFactory().getAccountDAO();

    private static final Map<String, Integer> sortingBy = new HashMap<>();

    static {
        sortingBy.put("id", AccountDAO.ID_ORDER);
        sortingBy.put("name", AccountDAO.NAME_ORDER);
        sortingBy.put("balance", AccountDAO.BALANCE_ORDER);
    }

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        super.execute(request, response);
        return new Pair<>(Status.FORWARD, PathConstants.CUSTOMER_ACCOUNTS_PATH);
    }

    @Override
    protected int countResource(int id) throws DaoException {
        return accountDAO.countAccountsByCustomerId(id);
    }

    @Override
    protected List<?> getResource(int id, int page, String sorting, String order, HttpServletRequest request)
            throws DaoException {
        logger.debug("Get account for customer with id: {}, page: {}, sorting: {}, order: {}",
                id, page, sorting, order);
        return accountDAO.getAccountsByCustomerIdOrderedBy(id, (page - 1) * MAX_ELEMENTS_ON_PAGE,
                MAX_ELEMENTS_ON_PAGE, sortingBy.get(sorting), orderIn.get(order));
    }

    @Override
    protected boolean containsSorting(String sorting) {
        return sortingBy.containsKey(sorting);
    }
}
