package com.epam.training.danychivan.command;

import com.epam.training.danychivan.db.dao.PaymentDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Customer;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class GetResourceCommand extends Command {

    protected static final int MAX_ELEMENTS_ON_PAGE = 3;

    protected static final Map<String, Integer> orderIn = new HashMap<>();

    protected static final String DEFAULT_SORTING = "id";

    static {
        orderIn.put("asc", PaymentDAO.ORDER_ASC);
        orderIn.put("desc", PaymentDAO.ORDER_DESC);
    }

    @Override
    public Pair<Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        HttpSession session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer");
        int page = Integer.parseInt(Optional
                .ofNullable(request.getParameter("page"))
                .orElse("1"));
        String sorting = Optional.ofNullable(request.getParameter("sorting"))
                .filter(this::containsSorting)
                .orElse(getDefaultSorting());
        String order = Optional.ofNullable(request.getParameter("order"))
                .filter(orderIn::containsKey)
                .orElse("asc");
        List<?> resources = getResource(customer.getId(), page, sorting, order, request);
        request.setAttribute("resource", resources);
        request.setAttribute("page", page);
        request.setAttribute("sorting", sorting);
        request.setAttribute("order", order);
        request.setAttribute("elementsNum", countResource(customer.getId()));
        return null;
    }

    protected abstract int countResource(int id) throws DaoException;

    protected abstract List<?> getResource(int id, int page, String sorting, String order, HttpServletRequest request)
            throws DaoException;

    protected abstract boolean containsSorting(String sorting);

    protected String getDefaultSorting() {
        return DEFAULT_SORTING;
    }

}
