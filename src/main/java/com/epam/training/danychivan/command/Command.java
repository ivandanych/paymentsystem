package com.epam.training.danychivan.command;

import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.exception.ServerException;
import javafx.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
   Abstract Command class, which must be extended by all commands
 * @author ivandanych
 */
public abstract class Command {

    protected static final Logger logger = LogManager.getLogger(Command.class);

    public abstract Pair<Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException, ServerException;

    /**
     * Enum which represent three possible option for controller after command executing
     */
    public enum Status {
        FORWARD, REDIRECT, NOTHING
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
