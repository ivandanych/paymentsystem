package com.epam.training.danychivan.command.impl.payment;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.command.GetResourceCommand;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.PaymentDAO;
import com.epam.training.danychivan.db.dao.StatusDAO;
import com.epam.training.danychivan.db.dto.PaymentDto;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Payment;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class GetPaymentsCommand extends GetResourceCommand {

    private final PaymentDAO paymentDAO = DAOFactory.getDefaultFactory().getPaymentDAO();

    private final StatusDAO statusDAO = DAOFactory.getDefaultFactory().getStatusDAO();

    private static final Map<String, Integer> sortingBy = new HashMap<>();

    static {
        sortingBy.put("id", PaymentDAO.ID_ORDER);
        sortingBy.put("date", PaymentDAO.DATE_ORDER);
    }

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        super.execute(request, response);
        return new Pair<>(Status.FORWARD, PathConstants.CUSTOMER_PAYMENTS_PATH);
    }

    @Override
    protected int countResource(int id) throws DaoException {
        return paymentDAO.countPaymentsByCustomerId(id);
    }

    @Override
    protected List<?> getResource(int id, int page, String sorting, String order, HttpServletRequest request)
            throws DaoException {
        logger.debug("Get payments for customer with id: {}, page: {}, sorting: {}, order: {}",
                id, page, sorting, order);
        HttpSession session = request.getSession();
        Locale locale = Optional.ofNullable((Locale) session.getAttribute("locale"))
                .orElse(new Locale("en"));
        List<Payment> payments = paymentDAO.getPaymentsByCustomerIdOrderedBy(id, (page - 1) * MAX_ELEMENTS_ON_PAGE,
                MAX_ELEMENTS_ON_PAGE, sortingBy.get(sorting), orderIn.get(order));
        List<PaymentDto> result = new ArrayList<>();
        for (Payment p : payments) {
            result.add(new PaymentDto(p,
                    statusDAO.getStatusDescriptionByIdForLanguage(p.getStatusId(), locale.getLanguage())));
        }
        return result;
    }

    @Override
    protected boolean containsSorting(String sorting) {
        return sortingBy.containsKey(sorting);
    }
}
