package com.epam.training.danychivan.command.impl.payment;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.ConnectionPool;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.UtilService;
import com.epam.training.danychivan.db.dao.AccountDAO;
import com.epam.training.danychivan.db.dao.PaymentDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Account;
import com.epam.training.danychivan.db.model.Customer;
import com.epam.training.danychivan.db.model.Payment;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CreatePaymentCommand extends Command {

    private final AccountDAO accountDAO = DAOFactory.getDefaultFactory().getAccountDAO();
    private final PaymentDAO paymentDAO = DAOFactory.getDefaultFactory().getPaymentDAO();

    private static final int PREPARED_ID = 1;
    private static final int DONE_ID = 2;

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        HttpSession session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer");
        Payment payment = extractPayment(request);
        List<String> errors = validateData(payment, customer, request);
        boolean notSuccess = false;
        if (!errors.isEmpty() || (notSuccess = !insertPayment(payment))) {
            logger.info("Customer with id: {}, can't create payment: {}, because: {}",
                    customer.getId(), payment, errors);
            if (notSuccess) {
                errors.add("payment.create.some-problem");
                logger.warn("Some problem while inserting payment: {}", payment);
            }
            request.setAttribute("errors", errors);
            request.setAttribute("accounts", accountDAO.getNotBlockedAccountByCustomerId(customer.getId()));
            return new Pair<>(Status.FORWARD, PathConstants.CREATE_PAYMENT);
        }
        logger.debug("Customer with id: {}, successfully insert payment: {}", customer.getId(), payment);
        return new Pair<>(Status.REDIRECT, PathConstants.CREATE_PAYMENT + "?success=true");
    }

    private boolean insertPayment(Payment payment) throws DaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            connection.setAutoCommit(false);
            boolean isSuccess = paymentDAO.insertPayment(connection, payment);
            if (!isSuccess) {
                logger.warn("Some problem while inserting payment: {}", payment);
                accountDAO.rollback(connection);
                return false;       //?????????????
            }
            if (payment.getStatusId() == Payment.PREPARE_STATUS) {
                return true;
            }
            UtilService.updateFromAccount(connection, payment);
            UtilService.updateToAccount(connection, payment);
            connection.commit();
        } catch (SQLException | DaoException e) {       // ???????
            e.printStackTrace();
            accountDAO.rollback(connection);    // ???????????
            logger.error("Can't insert payment: {}", payment, e);
            throw new DaoException("Can't insert payment: " + payment, e);
        } finally {
            try {
                if(connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Can't set auto commit to true", e);
            }
            accountDAO.close(connection);
        }
        return true;
    }

    private List<String> validateData(Payment payment, Customer customer, HttpServletRequest request) throws DaoException {
        List<String> errors = new ArrayList<>();
        if (accountDAO.getAllAccountsByCustomerId(customer.getId())
                .stream()
                .map(Account::getId)
                .noneMatch(integer -> integer == payment.getFromAccountId())) {
            errors.add("payment.create.account-error");
            return errors;
        }
        if (payment.getFromAccountId() == payment.getToAccountId()) {
            errors.add("payment.create.same-account-error");
        }
        if (accountDAO.getAccountById(payment.getToAccountId()) == null) {
            errors.add("payment.create.not-exist-error");
        }
        Account account = accountDAO.getAccountById(payment.getFromAccountId());
        BigDecimal balance = account.getBalance();
        BigDecimal value = payment.getValue();
        if ((value.compareTo(balance) > 0 && payment.getStatusId() == DONE_ID) ||
                value.compareTo(BigDecimal.ZERO) <= 0) {
            errors.add("payment.create.value-error");
        }
        if (account.isBlocked()) {
            errors.add("payment.create.blocked-error");
        }
        return errors;
    }

    private Payment extractPayment(HttpServletRequest request) {
        Payment payment = new Payment();
        String status;
        payment.setFromAccountId(Integer.parseInt(request.getParameter("fromAccountId")));
        payment.setToAccountId(Integer.parseInt(request.getParameter("toAccountId")));
        payment.setValue(BigDecimal.valueOf(Double.parseDouble(request.getParameter("value"))));
        status = Optional.ofNullable(request.getParameter("status"))
                .filter(s -> s.equalsIgnoreCase("prepare") || s.equalsIgnoreCase("send"))
                .orElse("prepare");
        if (status.equalsIgnoreCase("prepare")) {
            payment.setStatusId(PREPARED_ID);
        } else {
            payment.setStatusId(DONE_ID);
        }
        return payment;
    }
}
