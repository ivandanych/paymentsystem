package com.epam.training.danychivan.command.impl.account;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.command.GetResourceCommand;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.AccountDAO;
import com.epam.training.danychivan.db.dao.CustomerDAO;
import com.epam.training.danychivan.db.dto.AccountCustomerDto;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Account;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetAccountsForAdminCommand extends GetResourceCommand {

    protected static final int MAX_ELEMENTS_ON_PAGE = 15;

    private final CustomerDAO customerDAO = DAOFactory.getDefaultFactory().getCustomerDAO();
    private final AccountDAO accountDAO = DAOFactory.getDefaultFactory().getAccountDAO();

    private static final Map<String, Integer> sortingBy = new HashMap<>();

    static {
        sortingBy.put("id", AccountDAO.ID_ORDER);
        sortingBy.put("name", AccountDAO.NAME_ORDER);
        sortingBy.put("balance", AccountDAO.BALANCE_ORDER);
    }

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        super.execute(request, response);
        return new Pair<>(Status.FORWARD, PathConstants.ADMIN_ACCOUNTS_PATH);
    }

    @Override
    protected int countResource(int id) throws DaoException {
        return accountDAO.countAccounts();
    }

    @Override
    protected List<?> getResource(int id, int page, String sorting, String order, HttpServletRequest request)
            throws DaoException {
        logger.debug("Get account for admin with id: {}, page: {}, sorting: {}, order: {}",
                id, page, sorting, order);
        List<Account> accounts = accountDAO.getAccountsOrderedBy((page - 1) * MAX_ELEMENTS_ON_PAGE,
                MAX_ELEMENTS_ON_PAGE, sortingBy.get(sorting), orderIn.get(order));
        List<AccountCustomerDto> resources = new ArrayList<>();
        for (Account account : accounts) {
            resources.add(new AccountCustomerDto(account, customerDAO.getCustomerById(account.getCustomerId())));
        }
        return resources;
    }

    @Override
    protected boolean containsSorting(String sorting) {
        return sortingBy.containsKey(sorting);
    }

    @Override
    protected String getDefaultSorting() {
        return super.getDefaultSorting();
    }
}
