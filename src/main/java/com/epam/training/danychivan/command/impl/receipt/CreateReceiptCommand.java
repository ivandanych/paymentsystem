package com.epam.training.danychivan.command.impl.receipt;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.AccountDAO;
import com.epam.training.danychivan.db.dao.ReceiptDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Account;
import com.epam.training.danychivan.db.model.Customer;
import com.epam.training.danychivan.db.model.Receipt;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CreateReceiptCommand extends Command {

    private final AccountDAO accountDAO = DAOFactory.getDefaultFactory().getAccountDAO();
    private final ReceiptDAO receiptDAO = DAOFactory.getDefaultFactory().getReceiptDAO();

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        HttpSession session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer");
        Receipt receipt = extractReceipt(request);
        List<String> errors = validateData(receipt);
        boolean notSuccess = false;
        if (!errors.isEmpty() || (notSuccess = !receiptDAO.insertReceipt(receipt)) ) {
            logger.info("Customer with id: {}, can't create receipt: {}, because: {}",
                    customer.getId(), receipt, errors);
            if (notSuccess) {
                errors.add("receipt.create.some-problem");
                logger.warn("Some problem with inserting new receipt: {}", receipt);
            }
            request.setAttribute("errors", errors);
            List<Account> customerAccounts = accountDAO.getAllAccountsByCustomerId(customer.getId());
            customerAccounts.sort(Comparator.comparingInt(Account::getId));
            request.setAttribute("accounts", customerAccounts);
            return new Pair<>(Status.FORWARD, PathConstants.CUSTOMER_PAYMENTS_PATH);
        }
        logger.debug("Customer with id: {}, successfully create new receipt: {}", customer.getId(), receipt);
        return new Pair<>(Status.REDIRECT, PathConstants.CREATE_RECEIPT + "?success=true");
    }

    private List<String> validateData(Receipt receipt) throws DaoException {
        List<String> errors = new ArrayList<>();
        BigDecimal value = receipt.getValue();
        if(accountDAO.getAccountById(receipt.getToAccountId()) == null) {
            errors.add("receipt.create.not-exist-error");
        }
        if(value.compareTo(BigDecimal.ZERO) <= 0) {
            errors.add("receipt.create.value-error");
        }
        return errors;
    }

    private Receipt extractReceipt(HttpServletRequest request) {
        Receipt payment = new Receipt();
        payment.setToAccountId(Integer.parseInt(request.getParameter("toAccountId")));
        payment.setValue(BigDecimal.valueOf(Double.parseDouble(request.getParameter("value"))));
        return payment;
    }
}
