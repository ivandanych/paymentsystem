package com.epam.training.danychivan.command.impl.authorization;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.CustomerDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Customer;
import com.epam.training.danychivan.util.Util;
import com.epam.training.danychivan.util.validation.*;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RegisterCommand extends Command {

    private final CustomerDAO customerDAO = DAOFactory.getDefaultFactory().getCustomerDAO();

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DaoException {
        if(request.getSession().getAttribute("customer") != null) {
            return new Pair<>(Status.REDIRECT, PathConstants.PROFILE_PATH);
        }
        Customer customer = extractCustomerFromRequest(request);
        List<String> errors = validateData(customer, request);
        customer.setPassword(Util.hashString(customer.getPassword()));
        boolean notSuccess = false;
        if (!errors.isEmpty() || (notSuccess = !customerDAO.insertCustomer(customer))) {
            logger.info("Customer: {}, can't registered: {}", customer, errors);
            if (notSuccess) {
                errors.add("registration.some-problem");
                logger.warn("Some problem with inserting new customer: {}", customer);
            }
            request.setAttribute("errors", errors);
            return new Pair<>(Status.FORWARD, PathConstants.REGISTRATION_PATH);
        }
        logger.debug("Customer: {}, was successfully registered", customer);
        return new Pair<>(Status.REDIRECT, PathConstants.LOGIN_PATH + "?success=true");
    }

    private List<String> validateData(Customer customer, HttpServletRequest request) throws DaoException {
        List<String> errors = new ArrayList<>();
        String passwordConfirm = request.getParameter("passwordConfirm");

        MultiplyValidator.validate(customer.getName(), new NameValidator(), errors);
        MultiplyValidator.validate(customer.getSurname(), new SurnameValidator(), errors);
        MultiplyValidator.validate(customer.getLogin(), new LoginValidator(), errors);
        MultiplyValidator.validate(customer.getEmail(), new EmailValidator(), errors);
        MultiplyValidator.validate(customer.getPassword(), new PasswordValidator(), errors);

        if(passwordConfirm == null || !passwordConfirm.equals(customer.getPassword())) {
            errors.add("registration.password_match_alert");
        }
        if (customerDAO.getCustomerByLogin(customer.getLogin()) != null) {
            errors.add("registration.login-exist");
        }
        if (customerDAO.getCustomerByEmail(customer.getEmail()) != null) {
            errors.add("registration.email-exist");
        }
        return errors;
    }

    private Customer extractCustomerFromRequest(HttpServletRequest request) {
        Customer customer = new Customer();
        customer.setName(request.getParameter("name"));
        customer.setSurname(request.getParameter("surname"));
        customer.setLogin(request.getParameter("login"));
        customer.setEmail(request.getParameter("email"));
        customer.setPassword(request.getParameter("password"));
        return customer;
    }

}
