package com.epam.training.danychivan.command.impl.receipt;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.command.GetCreateResourceCommand;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Account;
import com.epam.training.danychivan.db.model.Customer;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

public class GetCreateReceiptCommand extends GetCreateResourceCommand {

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        super.execute(request, response);
        return new Pair<>(Status.FORWARD, PathConstants.CREATE_RECEIPT);
    }

    @Override
    protected List<Account> getCustomerAccounts(Customer customer) throws DaoException {
        List<Account> accounts = accountDAO.getAllAccountsByCustomerId(customer.getId());
        accounts.sort(Comparator.comparingInt(Account::getId));
        return accounts;
    }
}
