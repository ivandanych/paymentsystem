package com.epam.training.danychivan.command.impl.account;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.AccountDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Account;
import com.epam.training.danychivan.db.model.Customer;
import javafx.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class BlockAccount extends Command {

    private final AccountDAO accountDAO = DAOFactory.getDefaultFactory().getAccountDAO();

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        String result;
        int id = Integer.parseInt(request.getParameter("id"));
        Customer customer = (Customer) request.getSession().getAttribute("customer");
        List<Account> accountList = accountDAO.getAllAccountsByCustomerId(customer.getId());
        if (accountList.stream()
                .map(Account::getId)
                .anyMatch(integer -> integer == id)) {
            logger.debug("Customer: " + customer + " block account with id: " + id);
            accountDAO.blockOrUnBlockAccountById(id);
            result = request.getRequestURI() + "?" + request.getQueryString();
        } else {
            logger.warn("Customer: " + customer + " trying block not own account!");
            result = PathConstants.PERMISSION_ERROR;
        }
        logger.debug("Account with id: " + id + " was successfully blocked");
        return new Pair<>(Status.REDIRECT, result);
    }
}
