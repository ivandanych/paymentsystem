package com.epam.training.danychivan.command.impl.request;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.command.GetResourceCommand;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.UnblockRequestDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetUnblockRequestsCommand extends GetResourceCommand {

    protected static final int MAX_ELEMENTS_ON_PAGE = 15;

    protected static final String DEFAULT_SORTING = "date";

    private final UnblockRequestDAO unblockRequestDAO = DAOFactory.getDefaultFactory().getUnblockRequestDAO();

    private static final Map<String, Integer> sortingBy = new HashMap<>();

    static {
        sortingBy.put("date", UnblockRequestDAO.DATE_ORDER);
        sortingBy.put("accountNumber", UnblockRequestDAO.ACCOUNT_ID_ORDER);
    }

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        super.execute(request, response);
        return new Pair<>(Status.FORWARD, PathConstants.ADMIN_UNBLOCK_REQUEST_PATH);
    }

    @Override
    protected int countResource(int id) throws DaoException {
        return unblockRequestDAO.countUnblockRequests();
    }

    @Override
    protected List<?> getResource(int id, int page, String sorting, String order, HttpServletRequest request)
            throws DaoException {
        logger.debug("Get unblock requests for admin with id: {}, page: {}, sorting: {}, order: {}",
                id, page, sorting, order);
        return unblockRequestDAO.getUnblockRequestsOrderedBy(
                (page - 1) * MAX_ELEMENTS_ON_PAGE, MAX_ELEMENTS_ON_PAGE, sortingBy.get(sorting), orderIn.get(order));
    }

    @Override
    protected boolean containsSorting(String sorting) {
        return sortingBy.containsKey(sorting);
    }

    @Override
    protected String getDefaultSorting() {
        return DEFAULT_SORTING;
    }
}
