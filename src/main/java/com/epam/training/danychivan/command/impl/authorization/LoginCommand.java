package com.epam.training.danychivan.command.impl.authorization;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.CustomerDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Customer;
import com.epam.training.danychivan.util.Util;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LoginCommand extends Command {

    private final CustomerDAO customerDAO = DAOFactory.getDefaultFactory().getCustomerDAO();

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        if(request.getSession().getAttribute("customer") != null) {
            return new Pair<>(Status.REDIRECT, PathConstants.PROFILE_PATH);
        }
        Customer customer = extractLoginData(request);
        List<String> errors = validateData(customer);
        if (!errors.isEmpty()) {
            logger.info("Customer: {}, can't login: {}", customer , errors);
            request.setAttribute("errors", errors);
            return new Pair<>(Status.FORWARD, PathConstants.LOGIN_PATH);
        }
        customer = customerDAO.getCustomerByLogin(customer.getLogin());
        HttpSession session = request.getSession();
        session.setAttribute("customer", customer);
        logger.debug("Customer: {}, was successfully logged", customer);
        return new Pair<>(Status.REDIRECT, PathConstants.PROFILE_PATH);
    }

    private List<String> validateData(Customer customer) throws DaoException {
        List<String> errors = new ArrayList<>();
        Customer customerInDb = customerDAO.getCustomerByLogin(customer.getLogin());
        if (customerInDb == null) {
            errors.add("login.customer-not-exist");
            return errors;
        }
        if (!customerInDb.getPassword().equals(customer.getPassword())) {
            errors.add("login.wrong-password");
        }
        if(customerInDb.isBlocked()) {
            errors.add("login.customer-blocked");
        }
        return errors;
    }

    private Customer extractLoginData(HttpServletRequest request) {
        Customer customer = new Customer();
        customer.setLogin(request.getParameter("login"));
        customer.setPassword(Util.hashString(request.getParameter("password")));
        return customer;
    }
}
