package com.epam.training.danychivan.command.impl.withdraw;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.AccountDAO;
import com.epam.training.danychivan.db.dao.WithdrawDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Account;
import com.epam.training.danychivan.db.model.Customer;
import com.epam.training.danychivan.db.model.Withdraw;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CreateWithdrawCommand extends Command {

    private final AccountDAO accountDAO = DAOFactory.getDefaultFactory().getAccountDAO();
    private final WithdrawDAO withdrawDAO = DAOFactory.getDefaultFactory().getWithdrawDAO();

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        HttpSession session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer");
        Withdraw withdraw = extractWithdraw(request);
        List<String> errors = validateData(withdraw,customer);
        boolean notSuccess = false;
        if (!errors.isEmpty() || (notSuccess = !withdrawDAO.insertWithdraw(withdraw)) ) {
            logger.info("Customer with id: {}, can't create withdraw: {}, because: {}",
                    customer.getId(), withdraw, errors);
            if (notSuccess) {
                errors.add("withdraw.create.some-problem");
                logger.warn("Some problem with inserting withdraw: {}", withdraw);
            }
            request.setAttribute("errors", errors);
            request.setAttribute("accounts", accountDAO.getNotBlockedAccountByCustomerId(customer.getId()));
            return new Pair<>(Status.FORWARD, PathConstants.CREATE_WITHDRAW);
        }
        logger.debug("Customer with id: {}, successfully created withdraw: {}", customer.getId(), withdraw);
        return new Pair<>(Status.REDIRECT, PathConstants.CREATE_WITHDRAW + "?success=true");
    }

    private List<String> validateData(Withdraw withdraw, Customer customer) throws DaoException {
        List<String> errors = new ArrayList<>();
        if (accountDAO.getAllAccountsByCustomerId(customer.getId())
                .stream()
                .map(Account::getId)
                .noneMatch(integer -> integer == withdraw.getFromAccountId())) {
            errors.add("payment.create.account-error");
            return errors;
        }
        Account account = accountDAO.getAccountById(withdraw.getFromAccountId());
        BigDecimal balance = account.getBalance();
        BigDecimal value = withdraw.getValue();
        if(value.compareTo(balance) > 0 ||  value.compareTo(BigDecimal.ZERO) <= 0) {
            errors.add("payment.create.value-error");
        }
        if(account.isBlocked()) {
            errors.add("payment.create.blocked-error");
        }
        return errors;
    }

    private Withdraw extractWithdraw(HttpServletRequest request) {
        Withdraw withdraw = new Withdraw();
        withdraw.setFromAccountId(Integer.parseInt(request.getParameter("fromAccountId")));
        withdraw.setValue(BigDecimal.valueOf(Double.parseDouble(request.getParameter("value"))));
        return withdraw;
    }
}
