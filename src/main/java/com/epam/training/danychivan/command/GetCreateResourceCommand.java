package com.epam.training.danychivan.command;

import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.AccountDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Account;
import com.epam.training.danychivan.db.model.Customer;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class GetCreateResourceCommand extends Command {

    protected final AccountDAO accountDAO = DAOFactory.getDefaultFactory().getAccountDAO();

    @Override
    public Pair<Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        HttpSession session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer");
        List<Account> customerAccounts = getCustomerAccounts(customer);
        request.setAttribute("accounts", customerAccounts);
        return null;
    }

    protected List<Account> getCustomerAccounts(Customer customer) throws DaoException {
        List<Account> customerAccounts = accountDAO.getAllAccountsByCustomerId(customer.getId());
        return customerAccounts.stream()
                .filter(account -> !account.isBlocked())
                .sorted(Comparator.comparingInt(Account::getId))
                .collect(Collectors.toList());
    }

}
