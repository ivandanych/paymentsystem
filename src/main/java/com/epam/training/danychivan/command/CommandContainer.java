package com.epam.training.danychivan.command;

import com.epam.training.danychivan.command.impl.*;
import com.epam.training.danychivan.command.impl.account.*;
import com.epam.training.danychivan.command.impl.authorization.LoginCommand;
import com.epam.training.danychivan.command.impl.authorization.LogoutCommand;
import com.epam.training.danychivan.command.impl.authorization.RegisterCommand;
import com.epam.training.danychivan.command.impl.customer.BlockUnblockCustomerCommand;
import com.epam.training.danychivan.command.impl.customer.GetCustomersCommand;
import com.epam.training.danychivan.command.impl.payment.*;
import com.epam.training.danychivan.command.impl.receipt.CreateReceiptCommand;
import com.epam.training.danychivan.command.impl.receipt.GetCreateReceiptCommand;
import com.epam.training.danychivan.command.impl.receipt.GetReceiptsCommand;
import com.epam.training.danychivan.command.impl.request.GetUnblockRequestsCommand;
import com.epam.training.danychivan.command.impl.withdraw.CreateWithdrawCommand;
import com.epam.training.danychivan.command.impl.withdraw.GetCreateWithdrawCommand;
import com.epam.training.danychivan.command.impl.withdraw.GetWithdrawsCommand;

import java.util.HashMap;
import java.util.Map;


/**
 * Class which contains all implemented commands
 * Commands divided to GET and POST types
 * Type of command correspondents to request method
 * @author Ivan Danych
 */
public class CommandContainer {

    private static Map<String, Command> postCommandMap = new HashMap<>();
    private static Map<String, Command> getCommandMap = new HashMap<>();

    static {
        postCommandMap.put("changeLanguage", new ChangeLanguageCommand());
        postCommandMap.put("register", new RegisterCommand());
        postCommandMap.put("login", new LoginCommand());
        postCommandMap.put("blockAccount", new BlockAccount());
        postCommandMap.put("createAccount", new CreateAccountCommand());
        postCommandMap.put("createPayment", new CreatePaymentCommand());
        postCommandMap.put("createReceipt", new CreateReceiptCommand());
        postCommandMap.put("createWithdraw", new CreateWithdrawCommand());
        postCommandMap.put("confirmPayment", new ConfirmPaymentCommand());
        postCommandMap.put("blockUnblockCustomer", new BlockUnblockCustomerCommand());
        postCommandMap.put("sendRequestUnblockAccount", new SendUnblockRequestCommand());
        postCommandMap.put("blockUnblockAccount", new BlockUnblockAccountCommand());
        postCommandMap.put("generatePdf", new GeneratePDFCommand());

        getCommandMap.put("/logout", new LogoutCommand());
        getCommandMap.put("/accounts", new GetAccountsCommand());
        getCommandMap.put("/payments", new GetPaymentsCommand());
        getCommandMap.put("/payments/create", new GetCreatePaymentCommand());
        getCommandMap.put("/receipts", new GetReceiptsCommand());
        getCommandMap.put("/receipts/create", new GetCreateReceiptCommand());
        getCommandMap.put("/withdraws", new GetWithdrawsCommand());
        getCommandMap.put("/withdraws/create", new GetCreateWithdrawCommand());
        getCommandMap.put("/admin/customers", new GetCustomersCommand());
        getCommandMap.put("/admin/accounts", new GetAccountsForAdminCommand());
        getCommandMap.put("/admin/unblock-requests", new GetUnblockRequestsCommand());

    }

    private CommandContainer() {

    }

    public static Command getPostCommand(String name) {
        return postCommandMap.get(name);
    }

    public static Command getGetCommand(String url) {
        return getCommandMap.get(url);
    }
}
