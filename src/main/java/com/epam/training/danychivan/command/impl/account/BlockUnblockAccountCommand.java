package com.epam.training.danychivan.command.impl.account;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.AccountDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BlockUnblockAccountCommand extends Command {

    private final AccountDAO accountDAO = DAOFactory.getDefaultFactory().getAccountDAO();

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        int id = Integer.parseInt(request.getParameter("id"));
        logger.debug("Admin block/unblock account with id: " + id);
        accountDAO.blockOrUnBlockAccountById(id);
        return new Pair<>(Status.REDIRECT, request.getRequestURI() + "?" + request.getQueryString());
    }

}
