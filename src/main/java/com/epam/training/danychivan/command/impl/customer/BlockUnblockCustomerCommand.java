package com.epam.training.danychivan.command.impl.customer;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.CustomerDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Customer;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BlockUnblockCustomerCommand extends Command {

    private final CustomerDAO customerDAO = DAOFactory.getDefaultFactory().getCustomerDAO();

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        int id = Integer.parseInt(request.getParameter("id"));
        Customer customer = customerDAO.getCustomerById(id);
        if(customer.getRoleId() == Customer.ADMIN_ROLE) {       // May produce NullPointerException
            logger.warn("Admin try to block another admin with id: {}", id);
            return new Pair<>(Status.REDIRECT, PathConstants.PERMISSION_ERROR);
        }
        logger.debug("Admin blocked/unblocked customer with id: {}", id);
        customerDAO.blockOrUnBlockCustomerById(id);
        return new Pair<>(Status.REDIRECT, request.getRequestURI() + "?" + request.getQueryString());
    }
}
