package com.epam.training.danychivan.command.impl;

import com.epam.training.danychivan.command.Command;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

public class ChangeLanguageCommand extends Command {

    @Override
    public Pair<Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Locale locale = new Locale(request.getParameter("locale"));
        session.setAttribute("locale", locale);
        return new Pair<>(Status.REDIRECT, request.getRequestURI() + "?" + request.getQueryString());
    }
}
