package com.epam.training.danychivan.command.impl.customer;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.command.GetResourceCommand;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.CustomerDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetCustomersCommand extends GetResourceCommand {

    protected static final int MAX_ELEMENTS_ON_PAGE = 15;

    protected static final String DEFAULT_SORTING = "surname";

    private final CustomerDAO customerDAO = DAOFactory.getDefaultFactory().getCustomerDAO();

    protected static final Map<String, Integer> sortingBy = new HashMap<>();

    static {
        sortingBy.put("surname", CustomerDAO.SURNAME_ORDER);
        sortingBy.put("blocking", CustomerDAO.BLOCKING_ORDER);
    }

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        super.execute(request, response);
        return new Pair<>(Status.FORWARD, PathConstants.ADMIN_CUSTOMERS_PATH);
    }

    @Override
    protected int countResource(int id) throws DaoException {
        return customerDAO.countCustomers();
    }

    @Override
    protected List<?> getResource(int id, int page, String sorting, String order,HttpServletRequest request)
            throws DaoException {
        logger.debug("Get customers for admin with id: {}, page: {}, sorting: {}, order: {}",
                id, page, sorting, order);
        return customerDAO.getCustomersOrderedBy((page - 1) * MAX_ELEMENTS_ON_PAGE,
                MAX_ELEMENTS_ON_PAGE, sortingBy.get(sorting), orderIn.get(order));
    }

    @Override
    protected boolean containsSorting(String sorting) {
        return  sortingBy.containsKey(sorting);
    }

    @Override
    protected String getDefaultSorting() {
        return DEFAULT_SORTING;
    }
}
