package com.epam.training.danychivan.command.impl.payment;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.dao.PaymentDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Customer;
import com.epam.training.danychivan.db.model.Payment;
import com.epam.training.danychivan.exception.GeneratePdfException;
import com.epam.training.danychivan.exception.ServerException;
import com.epam.training.danychivan.service.ReportsService;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.Locale;
import java.util.Optional;

public class GeneratePDFCommand extends Command {

    private static final int BUF_SIZE = 1048;

    private final PaymentDAO paymentDAO = DAOFactory.getDefaultFactory().getPaymentDAO();

    @Override
    public Pair<Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException, ServerException {
        int id = Integer.parseInt(request.getParameter("id"));
        Payment payment = paymentDAO.getPaymentById(id);
        HttpSession session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer");
        if (paymentDAO.getAllPaymentsByCustomerId(customer.getId())
                .stream()
                .filter(p -> p.getId() == id)
                .findFirst()
                .isEmpty()) {
            logger.warn("Customer with id: {}, trying generate pdf for not own payment with id: {}",
                    customer.getId(), id);
            return new Pair<>(Status.REDIRECT, PathConstants.PERMISSION_ERROR);
        }
        Locale locale = Optional.ofNullable((Locale) session.getAttribute("locale"))
                .orElse(new Locale("en"));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            ReportsService.generatePaymentReport(payment, locale, outputStream);
        } catch (GeneratePdfException e) {
            logger.error("Can't generate pdf for payment: {}", payment);
            throw new ServerException("Can't generate report", e);
        }
        outputStream.close();
        response.setContentType("application/pdf");
        response.setHeader("Content-disposition",
                "attachment; filename=payment_report_" + payment.getId() + ".pdf");
        try (BufferedInputStream in = new BufferedInputStream(new ByteArrayInputStream(outputStream.toByteArray()));
             BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream())) {
            byte[] buffer = new byte[BUF_SIZE];
            int numBytesRead;
            while ((numBytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, numBytesRead);
            }
        }
        logger.debug("Pdf for payment with id: {}, was successfully generated", id);
        return new Pair<>(Status.NOTHING, null);
    }
}
