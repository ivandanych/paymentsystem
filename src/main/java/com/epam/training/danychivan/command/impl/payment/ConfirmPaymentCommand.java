package com.epam.training.danychivan.command.impl.payment;

import com.epam.training.danychivan.command.Command;
import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.ConnectionPool;
import com.epam.training.danychivan.db.DAOFactory;
import com.epam.training.danychivan.db.UtilService;
import com.epam.training.danychivan.db.dao.AccountDAO;
import com.epam.training.danychivan.db.dao.PaymentDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Account;
import com.epam.training.danychivan.db.model.Customer;
import com.epam.training.danychivan.db.model.Payment;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ConfirmPaymentCommand extends Command {

    PaymentDAO paymentDAO = DAOFactory.getDefaultFactory().getPaymentDAO();
    AccountDAO accountDAO = DAOFactory.getDefaultFactory().getAccountDAO();

    @Override
    public Pair<Command.Status, String> execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DaoException {
        HttpSession session = request.getSession();
        int id = Integer.parseInt(request.getParameter("id"));
        Customer customer = (Customer)session.getAttribute("customer");
        Optional<Payment> paymentOptional = paymentDAO.getAllPaymentsByCustomerId(customer.getId())
                .stream()
                .filter(p -> p.getId() == id)
                .findFirst();
        if(paymentOptional.isEmpty()) {
            logger.warn("Customer with id: {}, try to confirm not own payment with id: {}",
                    customer.getId(), id);
            return new Pair<>(Status.REDIRECT, PathConstants.PERMISSION_ERROR);
        }
        Payment payment = paymentOptional.get();
        List<String> errors = validateData(payment);
        payment.setStatusId(Payment.DONE_STATUS);
        boolean notSuccess = false;
        if (!errors.isEmpty() || (notSuccess = !updatePayment(payment)) ) {
            logger.info("Customer with id: {}, can't update payment with id: {}, because: {}",
                    customer.getId(), payment.getId(), errors);
            if (notSuccess) {
                errors.add("payments.confirm-some-problem");
                logger.warn("Some problem with update payment: {}", payment);
            }
            request.setAttribute("errors", errors);
            return new Pair<>(Status.FORWARD, PathConstants.ERROR_PAGE_PATH);
        }
        logger.debug("Customer with id: {}, successfully confirm payment: {}", customer.getId(), payment);
        return new Pair<>(Status.REDIRECT, request.getRequestURI() + "?" + request.getQueryString());
    }

    private boolean updatePayment(Payment payment) throws DaoException {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            connection.setAutoCommit(false);
            boolean isSuccess = paymentDAO.updatePayment(connection, payment);
            if (!isSuccess) {
                accountDAO.rollback(connection);
                logger.warn("Some problem while update payment: {}", payment);
                return false;       //?????????????
            }
            UtilService.updateFromAccount(connection, payment);
            UtilService.updateToAccount(connection, payment);
            connection.commit();
        } catch (SQLException | DaoException e) {
            e.printStackTrace();
            accountDAO.rollback(connection);    // ???????????
            logger.error("Can't insert payment: {}", payment, e);
            throw new DaoException("Can't insert payment: " + payment, e);
        } finally {
            try {
                if(connection != null) {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException e) {
                logger.error("Can't set auto commit to true", e);
            }
            accountDAO.close(connection);       // ?????????
        }
        return true;
    }

    private List<String> validateData(Payment payment) throws DaoException {
        List<String> errors = new ArrayList<>();
        Account account = accountDAO.getAccountById(payment.getFromAccountId());
        if(account.getBalance().compareTo(payment.getValue()) < 0) {
            errors.add("payments.confirm-value-error");
        }
        if(account.isBlocked()) {
            errors.add("payments.confirm-account-blocked");
        }
        return errors;
    }
}
