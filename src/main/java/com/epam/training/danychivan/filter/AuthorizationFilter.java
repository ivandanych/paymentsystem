package com.epam.training.danychivan.filter;

import com.epam.training.danychivan.constants.PathConstants;
import com.epam.training.danychivan.db.model.Customer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthorizationFilter implements Filter {

    private static final Logger logger = LogManager.getLogger(AuthorizationFilter.class);

    private static final int CUSTOMER_ID = 1;
    private static final int ADMIN_ID = 2;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        Customer customer = (Customer) session.getAttribute("customer");
        if(customer == null) {
            response.sendRedirect(PathConstants.LOGIN_PATH);
            return;
        }
        if(customer.getRoleId() < getPageRole(request)) {
            logger.debug("Some client tried to get resource which blocked for him");
            response.sendRedirect(PathConstants.HOME_PATH);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private int getPageRole(HttpServletRequest request) {
        if(request.getRequestURI().startsWith("/admin")) {
            return ADMIN_ID;
        }
        return CUSTOMER_ID;
    }
}
