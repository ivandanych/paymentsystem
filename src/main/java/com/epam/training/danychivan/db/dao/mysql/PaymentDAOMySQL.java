package com.epam.training.danychivan.db.dao.mysql;

import com.epam.training.danychivan.constants.DBConstants;
import com.epam.training.danychivan.db.DBManager;
import com.epam.training.danychivan.db.dao.DaoUtil;
import com.epam.training.danychivan.db.dao.PaymentDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Account;
import com.epam.training.danychivan.db.model.Payment;

import java.sql.*;
import java.util.List;

public class PaymentDAOMySQL implements PaymentDAO {

    private DBManager dbManager;

    private final DaoUtil<Payment> daoUtil;

    public PaymentDAOMySQL(DBManager dbManager) {
        this.dbManager = dbManager;
        daoUtil = new DaoUtil<>();
    }

    @Override
    public List<Payment> getAllPaymentsByCustomerId(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.getResourcesWithPreparedStatement(connection, DBConstants.GET_ALL_PAYMENTS_BY_CUSTOMER_ID,
                    this::extractPayment, id, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get all payments by customer id: " + id, e);
        }
    }

    @Override
    public Payment getPaymentById(int id) throws DaoException {
        Payment payment;
        try (Connection connection = dbManager.getConnection()) {
            List<Payment> payments = daoUtil.getResourcesWithPreparedStatement(connection,
                    DBConstants.GET_PAYMENT_BY_ID, this::extractPayment, id);
            if (payments.size() < 1) {
                return null;
            }
            payment = payments.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get payment with id: " + id, e);
        }
        return payment;
    }

    @Override
    public List<Payment> getAllPaymentByAccountId(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.getResourcesWithPreparedStatement(connection, DBConstants.GET_ALL_PAYMENTS_BY_ACCOUNT_ID,
                    this::extractPayment, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get all payments for account with id: " + id, e);
        }
    }

    @Override
    public List<Payment> getPaymentsByCustomerIdOrderedBy(int id, int offset, int limit, int sorting, int order)
            throws DaoException {
        try (Connection connection = dbManager.getConnection()){
            return daoUtil.getResourceOrderedBy(connection, DBConstants.GET_PAYMENTS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_ASC,
                    DBConstants.GET_PAYMENTS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_DESC, this::extractPayment,
                    order, id, id, sorting, offset, limit);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get payments order by for id: " + id, e);
        }
    }

    @Override
    public int countPaymentsByCustomerId(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.countResource(connection, DBConstants.COUNT_PAYMENTS_BY_CUSTOMER_ID, id, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't count payments for customer with id: " + id, e);
        }
    }

    @Override
    public boolean insertPayment(Connection connection, Payment payment) throws DaoException {
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(
                     DBConstants.INSERT_PAYMENT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, payment.getFromAccountId());
            statement.setInt(2, payment.getToAccountId());
            statement.setBigDecimal(3, payment.getValue());
            statement.setInt(4, payment.getStatusId());
            if (statement.executeUpdate() != 1) {
                return false;
            }
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                payment.setId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't insert payment: " + payment, e);
        } finally {
            close(resultSet);
        }
        return true;
    }

    @Override
    public boolean insertPayment(Payment payment) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return insertPayment(connection, payment);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get connection", e);
        }
    }

    @Override
    public boolean deletePaymentById(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.updateResource(connection, DBConstants.DELETE_PAYMENT_BY_ID, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't delete payment with id: " + id, e);
        }
    }

    @Override
    public boolean updatePayment(Connection connection, Payment payment) throws DaoException {
        try {
            return daoUtil.updateResource(connection, DBConstants.UPDATE_PAYMENT,
                    payment.getValue(), payment.getStatusId(), payment.getId());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't update payment: " + payment, e);
        }
    }

    @Override
    public boolean updatePayment(Payment payment) throws DaoException {
        try (Connection connection = dbManager.getConnection()){
            return updatePayment(connection, payment);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get connection", e);
        }
    }

    private Payment extractPayment(ResultSet resultSet) throws DaoException {
        Payment payment = new Payment();
        try {
            payment.setId(resultSet.getInt(1));
            payment.setFromAccountId(resultSet.getInt(2));
            payment.setToAccountId(resultSet.getInt(3));
            payment.setValue(resultSet.getBigDecimal(4));
            payment.setCreateDate(resultSet.getTimestamp(5));
            payment.setLastUpdateDate(resultSet.getTimestamp(6));
            payment.setStatusId(resultSet.getInt(7));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't extract payment from result set", e);
        }
        return payment;
    }
}