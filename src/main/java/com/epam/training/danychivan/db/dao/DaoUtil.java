package com.epam.training.danychivan.db.dao;

import com.epam.training.danychivan.db.exception.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DaoUtil<T> implements BaseDAO {

    public List<T> getResourcesWithStatement(Connection connection, String query, BaseDAO.Extracting<T> extract)
            throws SQLException, DaoException {
        List<T> resources = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {
            while (resultSet.next()) {
                resources.add(extract.extractResource(resultSet));
            }
        }
        return resources;
    }

    public List<T> getResourcesWithPreparedStatement(Connection connection, String query,
                                                      BaseDAO.Extracting<T> extract, Object... params)
            throws SQLException, DaoException {
        List<T> resources = new ArrayList<>();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                resources.add(extract.extractResource(resultSet));
            }
        } finally {
            close(resultSet);
        }
        return resources;
    }

    public List<T> getResourceOrderedBy(Connection connection, String queryAsc, String queryDesc,
                                         BaseDAO.Extracting<T> extracting, int order, Object... params)
            throws SQLException, DaoException {
        if (order == ORDER_ASC) {
            return getResourcesWithPreparedStatement(connection, queryAsc, extracting, params);
        }
        return getResourcesWithPreparedStatement(connection, queryDesc, extracting, params);
    }

    public int countResource(Connection connection, String query, Object... params) throws SQLException {
        int result = -1;
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }
        } finally {
            close(resultSet);
        }
        return result;
    }

    public boolean updateResource(Connection connection, String query, Object ... params) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            if (statement.executeUpdate() != 1) {
                return false;
            }
        }
        return true;
    }

}
