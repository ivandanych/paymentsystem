package com.epam.training.danychivan.db.dao.mysql;

import com.epam.training.danychivan.constants.DBConstants;
import com.epam.training.danychivan.db.DBManager;
import com.epam.training.danychivan.db.dao.DaoUtil;
import com.epam.training.danychivan.db.dao.ReceiptDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Receipt;

import java.sql.*;
import java.util.List;

public class ReceiptDAOMySQL implements ReceiptDAO {

    private DBManager dbManager;

    private final DaoUtil<Receipt> daoUtil;

    public ReceiptDAOMySQL(DBManager dbManager) {
        this.dbManager = dbManager;
        daoUtil = new DaoUtil<>();
    }

    @Override
    public List<Receipt> getAllReceiptsByCustomerId(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.getResourcesWithPreparedStatement(connection, DBConstants.GET_ALL_RECEIPTS_BY_CUSTOMER_ID,
                    this::extractReceipt, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get all receipts for customer with id: " + id, e);
        }
    }

    @Override
    public List<Receipt> getAllReceiptsByAccountId(int id) throws DaoException {

        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.getResourcesWithPreparedStatement(connection, DBConstants.GET_ALL_RECEIPTS_BY_ACCOUNT_ID,
                    this::extractReceipt, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get receipts for account with id: " + id, e);
        }
    }

    @Override
    public List<Receipt> getReceiptsByCustomerIdOrderedBy(int id, int offset, int limit, int sorting, int order) throws DaoException {

        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.getResourceOrderedBy(connection, DBConstants.GET_RECEIPTS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_ASC,
                    DBConstants.GET_RECEIPTS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_DESC, this::extractReceipt,
                    order, id, sorting, offset, limit);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get receipts order by for customer with id: " + id, e);
        }
    }

    @Override
    public int countReceiptsByCustomerId(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()){
            return daoUtil.countResource(connection, DBConstants.COUNT_RECEIPTS_BY_CUSTOMER_ID, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't count receipts for customer with id: " + id, e);
        }
    }

    @Override
    public boolean insertReceipt(Receipt receipt) throws DaoException {
        ResultSet resultSet = null;
        try (Connection connection = dbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     DBConstants.INSERT_RECEIPT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, receipt.getToAccountId());
            statement.setBigDecimal(2, receipt.getValue());
            if (statement.executeUpdate() != 1) {
                return false;
            }
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                receipt.setId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't insert receipt: " + receipt, e);
        } finally {
            close(resultSet);
        }
        return true;
    }

    @Override
    public boolean deleteReceiptById(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.updateResource(connection, DBConstants.DELETE_RECEIPT_BY_ID, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't delete receipt with id: " + id);
        }
    }

    private Receipt extractReceipt(ResultSet resultSet) throws DaoException {
        Receipt receipt = new Receipt();
        try {
            receipt.setId(resultSet.getInt(1));
            receipt.setToAccountId(resultSet.getInt(2));
            receipt.setValue(resultSet.getBigDecimal(3));
            receipt.setCreateDate(resultSet.getTimestamp(4));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't extract receipt from result set", e);
        }
        return receipt;
    }
}
