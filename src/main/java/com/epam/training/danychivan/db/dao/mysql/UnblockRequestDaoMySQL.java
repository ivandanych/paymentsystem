package com.epam.training.danychivan.db.dao.mysql;

import com.epam.training.danychivan.constants.DBConstants;
import com.epam.training.danychivan.db.DBManager;
import com.epam.training.danychivan.db.dao.DaoUtil;
import com.epam.training.danychivan.db.dao.UnblockRequestDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.UnblockRequest;

import java.sql.*;
import java.util.List;

public class UnblockRequestDaoMySQL implements UnblockRequestDAO {

    private DBManager dbManager;

    private final DaoUtil<UnblockRequest> daoUtil;

    public UnblockRequestDaoMySQL(DBManager dbManager) {
        this.dbManager = dbManager;
        daoUtil = new DaoUtil<>();
    }


    @Override
    public List<UnblockRequest> getUnblockRequestsOrderedBy(int offset, int limit, int sorting, int order) throws DaoException {
        try (Connection connection = dbManager.getConnection()){
            return daoUtil.getResourceOrderedBy(connection, DBConstants.GET_UNBLOCK_REQUEST_OFFSET_LIMIT_ORDER_BY_ASC,
                    DBConstants.GET_UNBLOCK_REQUEST_OFFSET_LIMIT_ORDER_BY_DESC, this::extractRequest,
                    order, sorting, offset, limit);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get unblock requests order by", e);
        }
    }

    @Override
    public boolean insertRequest(UnblockRequest unblockRequest) throws DaoException {
        ResultSet resultSet = null;
        try (Connection connection = dbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     DBConstants.INSERT_UNBLOCK_REQUEST, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, unblockRequest.getAccountId());
            if (statement.executeUpdate() != 1) {
                return false;
            }
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                unblockRequest.setId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't insert unblock request: " + unblockRequest, e);
        } finally {
            close(resultSet);
        }
        return true;
    }

    @Override
    public int countUnblockRequests() throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.countResource(connection, DBConstants.COUNT_UNBLOCK_REQUESTS);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't count unblock requests", e);
        }
    }

    @Override
    public int countNotCheckedRequestByAccountId(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.countResource(connection, DBConstants.COUNT_UNBLOCK_REQUESTS_BY_ACCOUNT_ID, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't count unblock requests for account with id: " + id, e);
        }
    }

    private UnblockRequest extractRequest(ResultSet resultSet) throws DaoException {
        UnblockRequest request = new UnblockRequest();
        try {
            request.setId(resultSet.getInt(1));
            request.setAccountId(resultSet.getInt(2));
            request.setChecked(resultSet.getBoolean(3));
            request.setCreateDate(resultSet.getTimestamp(4));
            request.setLastUpdate(resultSet.getTimestamp(5));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't extract request", e);
        }
        return request;
    }
}
