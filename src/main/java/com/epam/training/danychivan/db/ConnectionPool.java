package com.epam.training.danychivan.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Connection Pool use data source, tomcat implementation
 * It is singelton class
 * @author Ivan Danych
 */

public class ConnectionPool implements DBManager {

    private static final Logger logger = LogManager.getLogger(ConnectionPool.class);

    private static final String DATA_SOURCE_NAME = "jdbc/MySqlDataSource";

    private static ConnectionPool instance;

    private DataSource dataSource;

    private ConnectionPool() {
        try {
            InitialContext context = new InitialContext();
            Context envContext = (Context) context.lookup("java:/comp/env");
            dataSource = (DataSource) envContext.lookup(DATA_SOURCE_NAME);
        } catch (NamingException e) {
            e.printStackTrace();
            logger.fatal("App can't get dataSource from JNDI", e);
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public static ConnectionPool getInstance() {
        if (instance == null) {
            synchronized (ConnectionPool.class) {
                if (instance == null) {
                    instance = new ConnectionPool();
                }
            }
        }
        return instance;
    }
}
