package com.epam.training.danychivan.db.dao;

import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Withdraw;

import java.util.List;

public interface WithdrawDAO extends BaseDAO {

    int ID_ORDER = 1;
    int DATE_ORDER = 4;

    List<Withdraw> getAllWithdrawsByCustomerId(int id) throws DaoException;

    List<Withdraw> getAllWithdrawsByAccountId(int id) throws DaoException;

    List<Withdraw> getWithdrawsByCustomerIdOrderedBy(int id, int offset, int limit, int sorting, int order) throws DaoException;

    int countWithdrawsByCustomerId(int id) throws DaoException;

    boolean insertWithdraw(Withdraw withdraw) throws DaoException;

    boolean deleteWithdrawById(int id) throws DaoException;

}
