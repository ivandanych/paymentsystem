package com.epam.training.danychivan.db.dao;

import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Payment;
import com.epam.training.danychivan.db.model.Receipt;

import java.util.List;

public interface ReceiptDAO extends BaseDAO {

    int ID_ORDER = 1;
    int DATE_ORDER = 4;

    List<Receipt> getAllReceiptsByCustomerId(int id) throws DaoException;

    List<Receipt> getAllReceiptsByAccountId(int id) throws DaoException;

    List<Receipt> getReceiptsByCustomerIdOrderedBy(int id, int offset, int limit, int sorting, int order) throws DaoException;

    int countReceiptsByCustomerId(int id) throws DaoException;

    boolean insertReceipt(Receipt receipt) throws DaoException;

    boolean deleteReceiptById(int id) throws DaoException;
}
