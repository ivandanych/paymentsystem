package com.epam.training.danychivan.db.dao.mysql;

import com.epam.training.danychivan.constants.DBConstants;
import com.epam.training.danychivan.db.DBManager;
import com.epam.training.danychivan.db.dao.CustomerDAO;
import com.epam.training.danychivan.db.dao.DaoUtil;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Customer;

import java.sql.*;
import java.util.List;

public class CustomerDAOMySQL implements CustomerDAO {

    private DBManager dbManager;

    private final DaoUtil<Customer> daoUtil;

    public CustomerDAOMySQL(DBManager dbManager) {
        this.dbManager = dbManager;
        daoUtil = new DaoUtil<>();
    }

    public List<Customer> getAllCustomers() throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.getResourcesWithStatement(connection, DBConstants.GET_ALL_CUSTOMERS, this::extractCustomer);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get all customers", e);
        }
    }

    @Override
    public Customer getCustomerById(int id) throws DaoException {
        Customer customer;
        try (Connection connection = dbManager.getConnection()) {
            List<Customer> customers = daoUtil.getResourcesWithPreparedStatement(connection, DBConstants.GET_CUSTOMER_BY_ID,
                    this::extractCustomer, id);
            if (customers.size() < 1) {
                return null;
            }
            customer = customers.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get customer with id: " + id, e);
        }
        return customer;
    }

    @Override
    public Customer getCustomerByLogin(String login) throws DaoException {
        Customer customer;
        try (Connection connection = dbManager.getConnection()) {
            List<Customer> customers = daoUtil.getResourcesWithPreparedStatement(connection,
                    DBConstants.GET_CUSTOMER_BY_LOGIN, this::extractCustomer, login);
            if (customers.size() < 1) {
                return null;
            }
            customer = customers.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get customer with login: " + login, e);
        }
        return customer;
    }

    @Override
    public Customer getCustomerByEmail(String email) throws DaoException {
        Customer customer;
        try (Connection connection = dbManager.getConnection()) {
            List<Customer> customers = daoUtil.getResourcesWithPreparedStatement(connection,
                    DBConstants.GET_CUSTOMER_BY_EMAIL, this::extractCustomer, email);
            if (customers.size() < 1) {
                return null;
            }
            customer = customers.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get customer with email: " + email, e);
        }
        return customer;
    }

    @Override
    public List<Customer> getCustomersOrderedBy(int offset, int limit, int sorting, int order) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.getResourceOrderedBy(connection, DBConstants.GET_CUSTOMERS_OFFSET_LIMIT_ORDER_BY_ASC,
                    DBConstants.GET_CUSTOMERS_OFFSET_LIMIT_ORDER_BY_DESC, this::extractCustomer,
                    order, sorting, offset, limit);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get customers order by", e);
        }
    }

    @Override
    public int countCustomers() throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.countResource(connection, DBConstants.COUNT_CUSTOMERS);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't count customers", e);
        }
    }

    @Override
    public boolean insertCustomer(Customer customer) throws DaoException {
        ResultSet resultSet = null;
        try (Connection connection = dbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     DBConstants.INSERT_CUSTOMER, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, customer.getLogin());
            statement.setString(2, customer.getPassword());
            statement.setString(3, customer.getEmail());
            statement.setString(4, customer.getName());
            statement.setString(5, customer.getSurname());
            if (statement.executeUpdate() != 1) {
                return false;
            }
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                customer.setId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't insert customer: " + customer, e);
        } finally {
            close(resultSet);
        }
        return true;
    }

    @Override
    public boolean deleteCustomerByLogin(String login) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.updateResource(connection, DBConstants.DELETE_CUSTOMER_BY_LOGIN, login);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't delete customer with login: " + login, e);
        }
    }

    @Override
    public boolean blockOrUnBlockCustomerById(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.updateResource(connection, DBConstants.BLOCK_UNBLOCK_CUSTOMER_BY_ID, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't block/unblock customer with id: " + id, e);
        }
    }


    private Customer extractCustomer(ResultSet resultSet) throws DaoException {
        Customer customer = new Customer();
        try {
            customer.setId(resultSet.getInt(1));
            customer.setLogin(resultSet.getString(2));
            customer.setPassword(resultSet.getString(3));
            customer.setEmail(resultSet.getString(4));
            customer.setName(resultSet.getString(5));
            customer.setSurname(resultSet.getString(6));
            customer.setRoleId(resultSet.getInt(7));
            customer.setBlocked(resultSet.getBoolean(8));
            customer.setCreateDate(resultSet.getTimestamp(9));
            customer.setLastUpdateDate(resultSet.getTimestamp(10));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't extract customer", e);
        }
        return customer;
    }
}