package com.epam.training.danychivan.db.dao;

import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Customer;
import com.epam.training.danychivan.db.model.Payment;

import java.util.List;

public interface CustomerDAO extends BaseDAO {

    int BLOCKING_ORDER = 8;
    int SURNAME_ORDER = 6;

    List<Customer> getAllCustomers() throws DaoException;

    Customer getCustomerById(int id) throws DaoException;

    Customer getCustomerByLogin(String login) throws DaoException;

    Customer getCustomerByEmail(String email) throws DaoException;

    List<Customer> getCustomersOrderedBy(int offset, int limit, int sorting, int order) throws DaoException;

    int countCustomers() throws DaoException;

    boolean insertCustomer(Customer customer) throws DaoException;

    boolean deleteCustomerByLogin(String login) throws DaoException;

    boolean blockOrUnBlockCustomerById(int id) throws DaoException;

}
