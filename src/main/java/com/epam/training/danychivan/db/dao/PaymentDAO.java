package com.epam.training.danychivan.db.dao;

import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Payment;

import java.sql.Connection;
import java.util.List;

public interface PaymentDAO extends BaseDAO {

    int ID_ORDER = 1;
    int DATE_ORDER = 6;

    List<Payment> getAllPaymentsByCustomerId(int id) throws DaoException;

    Payment getPaymentById(int id) throws DaoException;

    List<Payment> getAllPaymentByAccountId(int id) throws DaoException;

    List<Payment> getPaymentsByCustomerIdOrderedBy(int id, int offset, int limit, int sorting, int order) throws DaoException;

    int countPaymentsByCustomerId(int id) throws DaoException;

    boolean insertPayment(Connection connection, Payment payment) throws DaoException;

    boolean insertPayment(Payment payment) throws DaoException;

    boolean deletePaymentById(int id) throws DaoException;

    boolean updatePayment(Connection connection, Payment payment) throws DaoException;

    boolean updatePayment(Payment payment) throws DaoException;

}
