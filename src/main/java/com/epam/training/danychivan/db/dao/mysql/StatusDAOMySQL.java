package com.epam.training.danychivan.db.dao.mysql;

import com.epam.training.danychivan.constants.DBConstants;
import com.epam.training.danychivan.db.DBManager;
import com.epam.training.danychivan.db.dao.DaoUtil;
import com.epam.training.danychivan.db.dao.StatusDAO;
import com.epam.training.danychivan.db.exception.DaoException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class StatusDAOMySQL implements StatusDAO {

    private DBManager dbManager;

    private DaoUtil<String> daoUtil;

    public StatusDAOMySQL(DBManager dbManager) {
        this.dbManager = dbManager;
        daoUtil = new DaoUtil<>();
    }

    @Override
    public String getStatusDescriptionByIdForLanguage(int id, String language) throws DaoException {
        String description;
        try (Connection connection = dbManager.getConnection()) {
            List<String> strings = daoUtil.getResourcesWithPreparedStatement(connection,
                    DBConstants.GET_STATUS_DESCRIPTION_BY_ID_AND_LANGUAGE, this::extractString, id, language);
            if (strings.size() < 1) {
                return null;
            }
            description = strings.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get status description for status with id: " + id +
                    " and with language: " + language, e);
        }
        return description;
    }

    private String extractString(ResultSet resultSet) throws DaoException {
        try {
            return resultSet.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't extract description from result set", e);
        }
    }

}
