package com.epam.training.danychivan.db.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Payment {

    public static final int PREPARE_STATUS = 1;

    public static final int DONE_STATUS = 2;

    protected int id;

    protected int fromAccountId;

    protected int toAccountId;

    protected BigDecimal value;

    protected Timestamp createDate;

    protected Timestamp lastUpdateDate;

    protected int statusId;

    public Payment() {

    }

    public Payment(Payment payment) {
        id = payment.id;
        fromAccountId = payment.fromAccountId;
        toAccountId = payment.toAccountId;
        value = payment.value;
        createDate = payment.createDate;
        lastUpdateDate = payment.lastUpdateDate;
        statusId = payment.statusId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFromAccountId() {
        return fromAccountId;
    }

    public void setFromAccountId(int fromAccountId) {
        this.fromAccountId = fromAccountId;
    }

    public int getToAccountId() {
        return toAccountId;
    }

    public void setToAccountId(int toAccountId) {
        this.toAccountId = toAccountId;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Timestamp lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", fromAccountId=" + fromAccountId +
                ", toAccountId=" + toAccountId +
                ", value=" + value +
                ", createDate=" + createDate +
                ", lastUpdateDate=" + lastUpdateDate +
                ", statusId=" + statusId +
                '}';
    }
}
