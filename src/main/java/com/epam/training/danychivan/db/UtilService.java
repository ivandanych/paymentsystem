package com.epam.training.danychivan.db;

import com.epam.training.danychivan.db.dao.AccountDAO;
import com.epam.training.danychivan.db.dao.CustomerDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Account;
import com.epam.training.danychivan.db.model.Payment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;

public class UtilService {

    private static final Logger logger = LogManager.getLogger(UtilService.class);

    private static final AccountDAO accountDAO = DAOFactory.getDefaultFactory().getAccountDAO();
    private static final CustomerDAO customerDAO = DAOFactory.getDefaultFactory().getCustomerDAO();

    private UtilService() {
    }

    public static boolean isCustomerBlocked(int id) {
        try {
            return customerDAO.getCustomerById(id).isBlocked();
        } catch (DaoException e) {
            logger.error("Can't get customer with id: {}, for checking blocking status", id);
        }
        return false;
    }

    public static void updateFromAccount(Connection connection, Payment payment) throws DaoException {
        Account from = accountDAO.getAccountById(payment.getFromAccountId());
        from.setBalance(from.getBalance().subtract(payment.getValue()));
        if (!accountDAO.updateAccountBalance(connection, from)) {
            throw new DaoException("Can't update from account for payment: " + payment);
        }
    }

    public static void updateToAccount(Connection connection, Payment payment) throws DaoException {
        Account to = accountDAO.getAccountById(payment.getToAccountId());
        to.setBalance(to.getBalance().add(payment.getValue()));
        if (!accountDAO.updateAccountBalance(connection, to)) {
            throw new DaoException("Can't update from account for payment: " + payment);
        }
    }

}
