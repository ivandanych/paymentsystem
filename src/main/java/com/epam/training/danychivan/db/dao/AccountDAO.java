package com.epam.training.danychivan.db.dao;

import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Account;

import java.sql.Connection;
import java.util.List;

public interface AccountDAO extends BaseDAO {

    int ID_ORDER = 1;
    int NAME_ORDER = 2;
    int BALANCE_ORDER = 3;

    List<Account> getAllAccounts() throws DaoException;

    Account getAccountById(int id) throws DaoException;

    List<Account> getAllAccountsByCustomerId(int id) throws DaoException;

    List<Account> getNotBlockedAccountByCustomerId(int id) throws DaoException;

    Account getAccountByCreditCard(String creditCard) throws DaoException;

    List<Account> getAccountsByCustomerIdOrderedBy(int id, int offset, int limit, int sorting, int order)
            throws DaoException;

    List<Account> getAccountsOrderedBy(int offset, int limit, int sorting, int order) throws DaoException;

    int countAccounts() throws DaoException;

    int countAccountsByCustomerId(int id) throws DaoException;

    boolean insertAccount(Account account) throws DaoException;

    boolean updateAccountBalance(Connection connection, Account account) throws DaoException;

    boolean updateAccountBalance(Account account) throws DaoException;

    boolean deleteAccountById(int id) throws DaoException;

    boolean blockOrUnBlockAccountById(int id) throws DaoException;

}
