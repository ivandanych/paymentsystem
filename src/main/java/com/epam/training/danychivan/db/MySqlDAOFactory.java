package com.epam.training.danychivan.db;

import com.epam.training.danychivan.db.dao.*;
import com.epam.training.danychivan.db.dao.mysql.*;

public class MySqlDAOFactory implements DAOFactory {

    private static MySqlDAOFactory instance;

    private DBManager dbManager;

    private MySqlDAOFactory(DBManager dbManager) {
        this.dbManager = dbManager;
    }

    public static MySqlDAOFactory getInstance() {
        if(instance == null) {
            synchronized (MySqlDAOFactory.class) {
                if(instance == null) {
                    instance = new MySqlDAOFactory(ConnectionPool.getInstance());
                }
            }
            instance = new MySqlDAOFactory(ConnectionPool.getInstance());
        }
        return instance;
    }

    @Override
    public CustomerDAO getCustomerDAO() {
        return new CustomerDAOMySQL(dbManager);
    }

    @Override
    public AccountDAO getAccountDAO() {
        return new AccountDAOMySQL(dbManager);
    }

    @Override
    public PaymentDAO getPaymentDAO() {
        return new PaymentDAOMySQL(dbManager);
    }

    @Override
    public ReceiptDAO getReceiptDAO() {
        return new ReceiptDAOMySQL(dbManager);
    }

    @Override
    public WithdrawDAO getWithdrawDAO() {
        return new WithdrawDAOMySQL(dbManager);
    }

    @Override
    public UnblockRequestDAO getUnblockRequestDAO() {
        return new UnblockRequestDaoMySQL(dbManager);
    }

    @Override
    public StatusDAO getStatusDAO() {
        return new StatusDAOMySQL(dbManager);
    }
}
