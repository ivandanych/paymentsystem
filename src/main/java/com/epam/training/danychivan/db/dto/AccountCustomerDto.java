package com.epam.training.danychivan.db.dto;

import com.epam.training.danychivan.db.model.Account;
import com.epam.training.danychivan.db.model.Customer;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class AccountCustomerDto {

    private int id;

    private String name;

    private BigDecimal balance;

    private String creditCardNumber;

    private boolean isBlocked;

    private String customerName;

    private String customerSurname;

    private Timestamp createDate;

    private Timestamp lastUpdate;

    public AccountCustomerDto() {
        isBlocked = false;
    }

    public AccountCustomerDto(Account account, Customer customer) {
        id = account.getId();
        name = account.getName();
        balance = account.getBalance();
        creditCardNumber = account.getCreditCardNumber();
        isBlocked = account.isBlocked();
        customerName = customer.getName();
        customerSurname = customer.getSurname();
        createDate = account.getCreateDate();
        lastUpdate = account.getLastUpdate();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerSurname() {
        return customerSurname;
    }

    public void setCustomerSurname(String customerSurname) {
        this.customerSurname = customerSurname;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
