package com.epam.training.danychivan.db;

import com.epam.training.danychivan.db.dao.*;

public interface DAOFactory {

    CustomerDAO getCustomerDAO();

    AccountDAO getAccountDAO();

    PaymentDAO getPaymentDAO();

    ReceiptDAO getReceiptDAO();

    WithdrawDAO getWithdrawDAO();

    UnblockRequestDAO getUnblockRequestDAO();

    StatusDAO getStatusDAO();

    static DAOFactory getDefaultFactory() {
        return MySqlDAOFactory.getInstance();
    }

}
