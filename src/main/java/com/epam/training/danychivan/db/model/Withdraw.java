package com.epam.training.danychivan.db.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Withdraw {

    private int id;

    private int fromAccountId;

    private BigDecimal value;

    private Timestamp createDate;

    public Withdraw() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFromAccountId() {
        return fromAccountId;
    }

    public void setFromAccountId(int fromAccountId) {
        this.fromAccountId = fromAccountId;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "Withdraw{" +
                "id=" + id +
                ", fromAccountId='" + fromAccountId + '\'' +
                ", value=" + value +
                ", createDate=" + createDate +
                '}';
    }
}
