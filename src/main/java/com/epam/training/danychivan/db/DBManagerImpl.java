package com.epam.training.danychivan.db;

import com.epam.training.danychivan.constants.DBConstants;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManagerImpl implements DBManager{


    private static final DBManagerImpl dbManager = new DBManagerImpl();

    private DBManagerImpl() {
    }

    public static DBManagerImpl getInstance() {
        return dbManager;
    }

    public Connection getConnection() {
        try {
            return DriverManager.getConnection(DBConstants.CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
