package com.epam.training.danychivan.db.dao;

import com.epam.training.danychivan.db.exception.DaoException;

public interface StatusDAO extends BaseDAO {

    String getStatusDescriptionByIdForLanguage(int id, String language) throws DaoException;

}
