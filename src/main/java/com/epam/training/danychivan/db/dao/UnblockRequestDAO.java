package com.epam.training.danychivan.db.dao;

import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.UnblockRequest;

import java.util.List;

public interface UnblockRequestDAO extends BaseDAO {

    int DATE_ORDER = 4;
    int ACCOUNT_ID_ORDER = 2;

    List<UnblockRequest> getUnblockRequestsOrderedBy(int offset, int limit, int sorting, int order) throws DaoException;

    boolean insertRequest(UnblockRequest unblockRequest) throws DaoException;

    int countUnblockRequests() throws DaoException;

    int countNotCheckedRequestByAccountId(int id) throws DaoException;
}
