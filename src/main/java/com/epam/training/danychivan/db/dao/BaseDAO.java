package com.epam.training.danychivan.db.dao;

import com.epam.training.danychivan.db.exception.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface BaseDAO {

    Logger logger = LogManager.getLogger(BaseDAO.class);

    int ORDER_ASC = 0;
    int ORDER_DESC = -1;

    default boolean close(AutoCloseable closeable) {
        try {
            if(closeable != null) {
                closeable.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    default void rollback(Connection connection) throws DaoException {
        try {
            if(connection != null) {
                connection.rollback();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't rollback transaction", e);
        }
    }

    interface Extracting<T> {
        T extractResource(ResultSet resultSet) throws DaoException;
    }

}
