package com.epam.training.danychivan.db.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Receipt {

    private int id;

    private int toAccountId;

    private BigDecimal value;

    private Timestamp createDate;

    public Receipt() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getToAccountId() {
        return toAccountId;
    }

    public void setToAccountId(int toAccountId) {
        this.toAccountId = toAccountId;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "Receipt{" +
                "id=" + id +
                ", toAccountId=" + toAccountId +
                ", value=" + value +
                ", createDate=" + createDate +
                '}';
    }
}
