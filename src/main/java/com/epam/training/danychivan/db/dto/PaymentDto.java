package com.epam.training.danychivan.db.dto;

import com.epam.training.danychivan.db.model.Payment;

public class PaymentDto extends Payment {

    String statusDescription;

    public PaymentDto(Payment payment, String statusDescription) {
        super(payment);
        this.statusDescription = statusDescription;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }
}
