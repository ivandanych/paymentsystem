package com.epam.training.danychivan.db;

import java.sql.Connection;
import java.sql.SQLException;

public interface DBManager {

    Connection getConnection() throws SQLException;

}
