package com.epam.training.danychivan.db.dao.mysql;

import com.epam.training.danychivan.constants.DBConstants;
import com.epam.training.danychivan.db.DBManager;
import com.epam.training.danychivan.db.dao.DaoUtil;
import com.epam.training.danychivan.db.dao.WithdrawDAO;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Withdraw;

import java.sql.*;
import java.util.List;

public class WithdrawDAOMySQL implements WithdrawDAO {

    private DBManager dbManager;

    private final DaoUtil<Withdraw> daoUtil;

    public WithdrawDAOMySQL(DBManager dbManager) {
        this.dbManager = dbManager;
        daoUtil = new DaoUtil<>();
    }

    @Override
    public List<Withdraw> getAllWithdrawsByCustomerId(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.getResourcesWithPreparedStatement(connection, DBConstants.GET_ALL_WITHDRAWS_BY_CUSTOMER_ID,
                    this::extractWithdraw, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get all withdraws fo ustomer with id: + " + id, e);
        }
    }

    @Override
    public List<Withdraw> getAllWithdrawsByAccountId(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.getResourcesWithPreparedStatement(connection, DBConstants.GET_ALL_WITHDRAWS_BY_ACCOUNT_ID,
                    this::extractWithdraw, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get all withdraws for account with id: " + id, e);
        }
    }

    @Override
    public List<Withdraw> getWithdrawsByCustomerIdOrderedBy(int id, int offset, int limit, int sorting, int order) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.getResourceOrderedBy(connection, DBConstants.GET_WITHDRAWS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_ASC,
                    DBConstants.GET_WITHDRAWS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_DESC, this::extractWithdraw,
                    order, id, sorting, offset, limit);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get withdraws ordered by for customer with id: " + id, e);
        }
    }

    @Override
    public int countWithdrawsByCustomerId(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.countResource(connection, DBConstants.COUNT_WITHDRAWS_BY_CUSTOMER_ID, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't count withdraws for customer with id: " + id, e);
        }
    }

    @Override
    public boolean insertWithdraw(Withdraw withdraw) throws DaoException {
        ResultSet resultSet = null;
        try (Connection connection = dbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     DBConstants.INSERT_WITHDRAW, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, withdraw.getFromAccountId());
            statement.setBigDecimal(2, withdraw.getValue());
            if (statement.executeUpdate() != 1) {
                return false;
            }
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                withdraw.setId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't insert withdraw: " + withdraw, e);
        } finally {
            close(resultSet);
        }
        return true;
    }

    @Override
    public boolean deleteWithdrawById(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.updateResource(connection, DBConstants.DELETE_WITHDRAW_BY_ID, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't delete withdraw with id: " + id, e);
        }
    }

    private Withdraw extractWithdraw(ResultSet resultSet) throws DaoException {
        Withdraw withdraw = new Withdraw();
        try {
            withdraw.setId(resultSet.getInt(1));
            withdraw.setFromAccountId(resultSet.getInt(2));
            withdraw.setValue(resultSet.getBigDecimal(3));
            withdraw.setCreateDate(resultSet.getTimestamp(4));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't ectract withdraw from result set", e);
        }
        return withdraw;
    }
}
