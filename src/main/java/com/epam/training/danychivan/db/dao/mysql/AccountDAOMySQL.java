package com.epam.training.danychivan.db.dao.mysql;

import com.epam.training.danychivan.constants.DBConstants;
import com.epam.training.danychivan.db.DBManager;
import com.epam.training.danychivan.db.dao.AccountDAO;
import com.epam.training.danychivan.db.dao.DaoUtil;
import com.epam.training.danychivan.db.exception.DaoException;
import com.epam.training.danychivan.db.model.Account;

import java.sql.*;
import java.util.List;

public class AccountDAOMySQL implements AccountDAO {

    private DBManager dbManager;

    private final DaoUtil<Account> daoUtil;

    public AccountDAOMySQL(DBManager dbManager) {
        this.dbManager = dbManager;
        daoUtil = new DaoUtil<>();
    }

    @Override
    public List<Account> getAllAccounts() throws DaoException {
        List<Account> result;
        try (Connection connection = dbManager.getConnection()) {
            result = daoUtil.getResourcesWithStatement(connection, DBConstants.GET_ALL_ACCOUNTS, this::extractAccount);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get all accounts", e);
        }
        return result;
    }

    @Override
    public List<Account> getAccountsByCustomerIdOrderedBy(int id, int offset, int limit, int sorting, int order)
            throws DaoException {
        List<Account> result;
        try (Connection connection = dbManager.getConnection()) {
            result = daoUtil.getResourceOrderedBy(connection, DBConstants.GET_ACCOUNTS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_ASC,
                    DBConstants.GET_ACCOUNTS_BY_CUSTOMER_OFFSET_LIMIT_ORDER_BY_DESC, this::extractAccount,
                    order, id, sorting, offset, limit);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get accounts", e);
        }
        return result;
    }

    @Override
    public Account getAccountById(int id) throws DaoException {
        Account account;
        try (Connection connection = dbManager.getConnection()) {
            List<Account> accounts = daoUtil.getResourcesWithPreparedStatement(connection, DBConstants.GET_ACCOUNT_BY_ID,
                    this::extractAccount, id);
            if (accounts.size() < 1) {
                return null;
            }
            account = accounts.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get account with id: " + id, e);
        }
        return account;
    }

    @Override
    public List<Account> getAllAccountsByCustomerId(int id) throws DaoException {
        List<Account> result;
        try (Connection connection = dbManager.getConnection()) {
            result = daoUtil.getResourcesWithPreparedStatement(connection, DBConstants.GET_ALL_ACCOUNTS_BY_CUSTOMER_ID,
                    this::extractAccount, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get all accounts for customer", e);
        }
        return result;
    }

    @Override
    public List<Account> getNotBlockedAccountByCustomerId(int id) throws DaoException {
        List<Account> result;
        try (Connection connection = dbManager.getConnection()) {
            result = daoUtil.getResourcesWithPreparedStatement(connection, DBConstants.GET_NOT_BLOCKED_ACCOUNTS_BY_CUSTOMER_ID,
                    this::extractAccount, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get not blocked accounts for customer", e);
        }
        return result;
    }

    @Override
    public Account getAccountByCreditCard(String creditCard) throws DaoException {
        Account account;
        try (Connection connection = dbManager.getConnection()) {
            List<Account> accounts = daoUtil.getResourcesWithPreparedStatement(connection,
                    DBConstants.GET_ACCOUNT_BY_CREDIT_CARD, this::extractAccount, creditCard);
            if (accounts.size() < 1) {
                return null;
            }
            account = accounts.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get account with credit card: " + creditCard, e);
        }
        return account;
    }

    @Override
    public List<Account> getAccountsOrderedBy(int offset, int limit, int sorting, int order) throws DaoException {
        List<Account> result;
        try (Connection connection = dbManager.getConnection()) {
            result = daoUtil.getResourceOrderedBy(connection, DBConstants.GET_ACCOUNTS_OFFSET_LIMIT_ORDER_BY_ASC,
                    DBConstants.GET_ACCOUNTS_OFFSET_LIMIT_ORDER_BY_DESC, this::extractAccount,
                    order, sorting, offset, limit);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get account from BD", e);
        }
        return result;
    }


    @Override
    public int countAccounts() throws DaoException {
        int res;
        try (Connection connection = dbManager.getConnection()) {
            res = daoUtil.countResource(connection, DBConstants.COUNT_ACCOUNTS);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't count accounts", e);
        }
        return res;
    }

    @Override
    public int countAccountsByCustomerId(int id) throws DaoException {
        int res;
        try (Connection connection = dbManager.getConnection()) {
            res = daoUtil.countResource(connection, DBConstants.COUNT_ACCOUNT_BY_CUSTOMER_ID, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't count account for customer with id " + id, e);
        }
        return res;
    }
    @Override
    public boolean insertAccount(Account account) throws DaoException {
        ResultSet resultSet = null;
        try (Connection connection = dbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     DBConstants.INSERT_ACCOUNT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, account.getName());
            statement.setString(2, account.getCreditCardNumber());
            statement.setInt(3, account.getCustomerId());
            if (statement.executeUpdate() != 1) {
                return false;
            }
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                account.setId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't insert account: " + account, e);
        } finally {
            close(resultSet);
        }
        return true;
    }

    @Override
    public boolean updateAccountBalance(Connection connection, Account account) throws DaoException {
        try {
            return daoUtil.updateResource(connection, DBConstants.UPDATE_BALANCE_ACCOUNT_BY_ID,
                    account.getBalance(), account.getId());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't update balance for account: " + account, e);
        }
    }

    @Override
    public boolean updateAccountBalance(Account account) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return updateAccountBalance(connection, account);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't get connection", e);
        }
    }

    @Override
    public boolean deleteAccountById(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.updateResource(connection, DBConstants.DELETE_ACCOUNT_BY_ID, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't update with id: " + id);
        }
    }

    @Override
    public boolean blockOrUnBlockAccountById(int id) throws DaoException {
        try (Connection connection = dbManager.getConnection()) {
            return daoUtil.updateResource(connection, DBConstants.BLOCK_UNBLOCK_ACCOUNT_BY_ID, id);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't block/unblock account with id: " + id, e);
        }
    }

    private Account extractAccount(ResultSet resultSet) throws DaoException {
        Account account = new Account();
        try {
            account.setId(resultSet.getInt(1));
            account.setName(resultSet.getString(2));
            account.setBalance(resultSet.getBigDecimal(3));
            account.setCreditCardNumber(resultSet.getString(4));
            account.setBlocked(resultSet.getBoolean(5));
            account.setCustomerId(resultSet.getInt(6));
            account.setCreateDate(resultSet.getTimestamp(7));
            account.setLastUpdate(resultSet.getTimestamp(8));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException("Can't extract resource from result set", e);
        }
        return account;
    }

}
