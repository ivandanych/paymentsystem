package com.epam.training.danychivan.service;

import com.epam.training.danychivan.db.model.Payment;
import com.epam.training.danychivan.exception.GeneratePdfException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class ReportsService {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm");

    private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 36,
            Font.BOLD);


    public static void generatePaymentReport(Payment payment, Locale locale, OutputStream outputStream)
            throws GeneratePdfException {
        ResourceBundle bundle = ResourceBundle.getBundle("locale", locale);
        Document document = new Document();
        try {
            PdfWriter.getInstance(document, outputStream);
            document.open();
            addTitle(document, bundle);
            addPaymentDescription(document, payment);
            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
            throw new GeneratePdfException("Can't generate pdf for payment: " + payment, e);
        }
    }

    private static void addPaymentDescription(Document document, Payment payment) throws DocumentException {
        Paragraph paymentDescription = new Paragraph();
        paymentDescription.add(new Paragraph("Payment number: " + payment.getId()));
        paymentDescription.add(new Paragraph("From account number: " + payment.getFromAccountId()));
        paymentDescription.add(new Paragraph("To account number: " + payment.getToAccountId()));
        paymentDescription.add(new Paragraph("Value: " + payment.getValue()));
        paymentDescription.add(new Paragraph("Date of creation payment: " +
                dateFormat.format(payment.getCreateDate())));
        paymentDescription.add(new Paragraph("Date of sent: " +
                dateFormat.format(payment.getLastUpdateDate())));
        paymentDescription.add(new Paragraph(""));
        paymentDescription.add(new LineSeparator());
        document.add(paymentDescription);
    }

    private static void addTitle(Document document, ResourceBundle bundle) throws DocumentException {
        Paragraph title = new Paragraph();
        Paragraph payPool = new Paragraph(
                "PayPool",
                titleFont
        );
        Paragraph createDate = new Paragraph(
                "Date of creation report: " + dateFormat.format(new Date())
        );
        createDate.setAlignment(Element.ALIGN_RIGHT);
        LineSeparator separator = new LineSeparator();
        title.add(payPool);
        title.add(createDate);
        title.add(separator);
        title.add(new Paragraph("\n"));
        document.add(title);
    }

}
