<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ tag language="java" pageEncoding="ISO-8859-1" %>
<%@ attribute name="curOrder" required="true" rtexprvalue="true" %>
<%@ attribute name="curSorting" required="true" rtexprvalue="true" %>


<c:choose>
    <c:when test="${curOrder == 'asc'}">
        <a class="nav-link" href="?sorting=${curSorting}&order=desc">
            &dArr;
        </a>
    </c:when>
    <c:otherwise>
        <a class="nav-link" href="?sorting=${curSorting}&order=asc">
            &uArr;
        </a>
    </c:otherwise>
</c:choose>