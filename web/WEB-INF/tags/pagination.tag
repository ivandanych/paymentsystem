<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>
<%@ tag language="java" pageEncoding="ISO-8859-1" %>
<%@ attribute name="elementsNum" required="true" rtexprvalue="true" %>
<%@ attribute name="elementsOnPage" required="true" rtexprvalue="true" %>


<nav aria-label="Page navigation">
    <c:set var="maxPageNum">
        <mytag:ceil param="${elementsNum/elementsOnPage}"/>
    </c:set>
    <ul class="pagination justify-content-center">
        <li class="page-item">
            <a class="page-link btn ${requestScope.page == 1 ? 'disabled' : ''}" aria-label="First"
               href="?sorting=${requestScope.sorting}&order=${requestScope.order}&page=1">
                <span aria-hidden="true">&larrb;</span>
            </a>
        </li>
        <li class="page-item">
            <a class="page-link btn ${requestScope.page == 1 ? 'disabled' : ''}" aria-label="Previous"
               href="?sorting=${requestScope.sorting}&order=${requestScope.order}&page=${requestScope.page - 1}">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">
                    <fmt:message key="pagination.prev"/>
                </span>
            </a>
        </li>
        <c:forEach begin="${requestScope.page - 5 <= 0 ? 1 : requestScope.page - 5}"
                   end="${requestScope.page - 1}" step="1" var="i">
            <li class="page-item">
                <a class="page-link" href="?sorting=${requestScope.sorting}&order=${requestScope.order}&page=${i}">
                        ${i}
                </a>
            </li>
        </c:forEach>
        <li class="page-item btn disabled p-0 m-0">
            <a class="page-link"
               href="?sorting=${requestScope.sorting}&order=${requestScope.order}&page=${requestScope.page}">
                ${requestScope.page}
            </a>
        </li>
        <c:forEach begin="${requestScope.page + 1}"
                   end="${requestScope.page + 5 >= maxPageNum ? maxPageNum : requestScope.page + 5}"
                   step="1" var="i">
            <li class="page-item">
                <a class="page-link" href="?sorting=${requestScope.sorting}&order=${requestScope.order}&page=${i}">
                        ${i}
                </a>
            </li>
        </c:forEach>
        <li class="page-item">
            <a class="page-link btn ${requestScope.page == maxPageNum ? 'disabled' : ''}" aria-label="Next"
               href="?sorting=${requestScope.sorting}&order=${requestScope.order}&page=${requestScope.page + 1}">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">
                    <fmt:message key="pagination.next"/>
                </span>
            </a>
        </li>
        <li class="page-item">
            <a class="page-link btn ${requestScope.page == maxPageNum ? 'disabled' : ''}" aria-label="Last"
               href="?sorting=${requestScope.sorting}&order=${requestScope.order}&page=${maxPageNum}">
                <span aria-hidden="true">&rarrb;</span>
            </a>
        </li>
    </ul>
</nav>