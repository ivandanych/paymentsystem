<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>

<html>

<%@include file="/WEB-INF/jspf/head.jspf" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/create-account.css">

<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>


<div class="container d-flex h-75 justify-content-center">
    <div class="row align-self-center">
        <div class="note">
            <p>
                <fmt:message key="account.create.title"/>
            </p>
        </div>

        <form class="form-content container" method="post">
            <div class="row">
                <div class="col-12 mb-2">
                    <label for="inputName" class="sr-only">
                        <fmt:message key="account.create.name"/>
                    </label>
                    <div class="mb-2">
                        <input id="inputName" type="text" class="form-control" name="name" value="${param.name}"
                               placeholder="<fmt:message key="registration.name"/>" required/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-2">
                    <label for="inputCreditCard" class="sr-only">
                        <fmt:message key="account.create.credit-card"/>
                    </label>
                    <div class="mb-2">
                        <input id="inputCreditCard" type="text" class="form-control" name="creditCard" value=""
                               placeholder="<fmt:message key="account.create.credit-card"/>"
                               pattern="[0-9]{16}" title="<fmt:message key="account.create.credit-card-alert"/>" required/>
                    </div>
                </div>
            </div>

            <c:if test="${not empty requestScope.errors}">
                <div class="alert alert-danger">
                    <c:forEach items="${requestScope.errors}" var="error">
                        <strong><fmt:message key="${error}"/></strong><br>
                    </c:forEach>
                </div>
            </c:if>

            <input type="hidden" name="command" value="createAccount"/>
            <input type="hidden" name="customerId" value="${sessionScope.customer.id}"/>
            <button type="submit" class="w-100 btn btn-lg btn-primary btn-block">
                <fmt:message key="account.create.button"/>
            </button>
        </form>
    </div>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>
<%@include file="/WEB-INF/jspf/bootstrapScript.jspf" %>
</body>
</html>