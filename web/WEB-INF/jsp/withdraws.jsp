<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>

<html>

<%@include file="/WEB-INF/jspf/head.jspf" %>
<link rel="stylesheet" href="style/accounts.css">

<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>

<div class="container d-flex h-75">
    <div class="col-3">
        <div class="row title">
            <h3 class="my-2 text-center">
                <fmt:message key="withdraws.menu-title"/>
            </h3>
        </div>
        <div class="card container">
            <div class="row align-items-center border-bottom border-dark">
                <div class="col-5 p-0 dropdown text-center border-right">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                       role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <fmt:message key="withdraws.menu-sorted-by"/>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="?sorting=id&order=${requestScope.order}">
                            <fmt:message key="withdraws.menu-sorting-number"/>
                        </a></li>
                        <li><a class="dropdown-item" href="?sorting=date&order=${requestScope.order}">
                            <fmt:message key="withdraws.menu-sorting-date"/>
                        </a></li>
                    </ul>
                </div>
                <div class="col-2 p-0 text-center border-right">
                    <c:choose>
                        <c:when test="${requestScope.order == 'asc'}">
                            <a class="nav-link" href="?sorting=${requestScope.sorting}&order=desc">
                                &dArr;
                            </a>
                        </c:when>
                        <c:otherwise>
                            <a class="nav-link" href="?sorting=${requestScope.sorting}&order=asc">
                                &uArr;
                            </a>
                        </c:otherwise>
                    </c:choose>
                </div>
                <h5 class="col-5 text-center m-0 p-0">
                    ${requestScope.sorting == "id" ? "number" : requestScope.sorting}
                </h5>
            </div>
            <div class="row align-items-center">
                <form class="col-12 m-0 text-center p-0" action="${pageContext.request.contextPath}/withdraws/create">
                    <button class="btn btn-primary w-100 rounded-0" type="submit">
                        <fmt:message key="withdraws.menu-create-payment"/>
                    </button>
                </form>
                <c:if test="${param.success}">
                    <div class="col-12 m-0 p-0 text-center align-items-center">
                        <h5>
                            <fmt:message key="withdraws.menu-create-account-success"/>
                        </h5>
                    </div>
                </c:if>
            </div>
        </div>
    </div>

    <div class="col-sm"></div>

    <div class="col-8 justify-content-center">
        <h3 class="my-2 text-center">
            <fmt:message key="withdraws.title"/>
        </h3>
        <div class="card container mb-4 border-2">
            <div class="row border-bottom border-dark">
                <div class="col-3 p-0 border-right">
                    <h5 class="text-center">
                        <fmt:message key="withdraws.withdraw-number"/>
                    </h5>
                </div>
                <div class="col-3 p-0 border-right">
                    <h5 class="text-center">
                        <fmt:message key="withdraws.from-account-number"/>
                    </h5>
                </div>
                <div class="col-3 p-0 border-right">
                    <h5 class="text-center">
                        <fmt:message key="withdraws.withdraw-value"/>
                    </h5>
                </div>
                <div class="col-3 p-0">
                    <h5 class="text-center">
                        <fmt:message key="withdraws.withdraw-send-date"/>
                    </h5>
                </div>
            </div>
            <c:forEach items="${requestScope.resource}" var="withdraw">
                <div class="row border-bottom border-dark">
                    <div class="col-3 p-0 border-right">
                        <p class="text-center m-0">${withdraw.id}</p>
                    </div>
                    <div class="col-3 p-0 border-right">
                        <p class="text-center m-0">${withdraw.fromAccountId}</p>
                    </div>
                    <div class="col-3 p-0 border-right">
                        <p class="text-center m-0">${withdraw.value}</p>
                    </div>
                    <div class="col-3 p-0 border-right">
                        <p class="text-center m-0">
                            <fmt:formatDate value="${withdraw.createDate}" pattern="dd/MM/yyyy"/>
                        </p>
                    </div>
                </div>
            </c:forEach>
        </div>
        <filetag:pagination elementsNum="${requestScope.elementsNum}" elementsOnPage="20"/>
    </div>

</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>
<%@include file="/WEB-INF/jspf/bootstrapScript.jspf" %>
</body>
</html>