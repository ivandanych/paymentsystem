<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>

<html>

<%@include file="/WEB-INF/jspf/head.jspf" %>
<link rel="stylesheet" href="style/accounts.css">

<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>

<div class="container d-flex h-75">
    <div class="col-3">
        <div class="row title">
            <h3 class="my-2 text-center">
                <fmt:message key="accounts.menu-title"/>
            </h3>
        </div>
        <div class="card container">
            <div class="row align-items-center border-bottom border-dark">
                <div class="col-5 p-0 dropdown text-center border-right">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                       role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <fmt:message key="accounts.menu-sorted-by"/>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="?sorting=id&order=${requestScope.order}">
                            <fmt:message key="accounts.menu-sorting-number"/>
                        </a></li>
                        <li><a class="dropdown-item" href="?sorting=name&order=${requestScope.order}">
                            <fmt:message key="accounts.menu-sorting-name"/>
                        </a></li>
                        <li><a class="dropdown-item" href="?sorting=balance&order=${requestScope.order}">
                            <fmt:message key="accounts.menu-sorting-balance"/>
                        </a></li>
                    </ul>
                </div>
                <div class="col-2 p-0 text-center border-right">
                    <c:choose>
                        <c:when test="${requestScope.order == 'asc'}">
                            <a class="nav-link" href="?sorting=${requestScope.sorting}&order=desc">
                                &dArr;
                            </a>
                        </c:when>
                        <c:otherwise>
                            <a class="nav-link" href="?sorting=${requestScope.sorting}&order=asc">
                                &uArr;
                            </a>
                        </c:otherwise>
                    </c:choose>
                </div>
                <h5 class="col-5 text-center m-0 p-0">
                    ${requestScope.sorting == "id" ? "number" : requestScope.sorting}
                </h5>
            </div>
            <div class="row align-items-center">
                <form class="col-12 m-0 text-center p-0" action="accounts/create">
                    <button class="btn btn-primary w-100 rounded-0" type="submit">
                        <fmt:message key="accounts.menu-create-account"/>
                    </button>
                </form>
                <c:if test="${param.success}">
                    <div class="col-12 m-0 p-0 text-center align-items-center">
                        <h5>
                            <fmt:message key="accounts.menu-create-account-success"/>
                        </h5>
                    </div>
                </c:if>
            </div>
        </div>
    </div>
    <div class="col-sm"></div>
    <div class="col-8 justify-content-center">
        <h3 class="my-2 text-center">
            <fmt:message key="accounts.title"/>
        </h3>
        <c:forEach items="${requestScope.resource}" var="account">
            <div class="card container mb-4 border-2 ${account.blocked ? "border-danger" : ""}">
                <div class="row">
                    <div class="col-3 p-0 border-bottom border-right border-dark">
                        <h5 class="text-center border-bottom border-dark">
                            <fmt:message key="accounts.account-number"/>
                        </h5>
                        <p class="m-0 text-center">${account.id}</p>
                    </div>
                    <div class="col-9 p-0 border-bottom border-dark">
                        <h5 class="text-center border-bottom border-dark">
                            <fmt:message key="accounts.account-name"/>
                        </h5>
                        <p class="m-0 text-center">${account.name}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 p-0 border-bottom border-right border-dark">
                        <h5 class="text-center border-bottom border-dark">
                            <fmt:message key="accounts.account-balance"/>
                        </h5>
                        <p class="text-center m-0">${account.balance} $</p>
                    </div>
                    <div class="col-9 p-0 border-bottom border-dark">
                        <h5 class="text-center border-bottom border-dark">
                            <fmt:message key="accounts.account-credit-card"/>
                        </h5>
                        <p class="text-center m-0">
                            <mytag:formatCreditCard creditCard="${account.creditCardNumber}" separator="-"/>
                        </p>
                    </div>
                </div>
                <div class="row border-bottom border-dark">
                    <form class="col-4 m-0 text-center p-0 border-right"
                          action="${pageContext.request.contextPath}/receipts/create">
                        <input type="hidden" name="accountNum" value="${account.id}"/>
                        <button class="btn btn-primary w-100 rounded-0 ${account.blocked ? "disabled" : ""}"
                                type="submit">
                            <fmt:message key="accounts.account-deposit"/>
                        </button>
                    </form>
                    <form class="col-4 m-0 text-center p-0 border-right"
                          action="${pageContext.request.contextPath}/payments/create">
                        <input type="hidden" name="accountNum" value="${account.id}"/>
                        <button class="btn btn-primary w-100 rounded-0 ${account.blocked ? "disabled" : ""}"
                                type="submit">
                            <fmt:message key="accounts.account-payment"/>
                        </button>
                    </form>
                    <form class="col-4 m-0 text-center p-0"
                          action="${pageContext.request.contextPath}/withdraws/create">
                        <input type="hidden" name="accountNum" value="${account.id}"/>
                        <button class="btn btn-primary w-100 rounded-0 ${account.blocked ? "disabled" : ""}"
                                type="submit">
                            <fmt:message key="accounts.account-withdraw"/>
                        </button>
                    </form>
                </div>
                <div class="row">
                    <c:choose>
                        <c:when test="${account.blocked}">
                            <form class="col-12 m-0 text-center p-0" method="post">
                                <input type="hidden" name="command" value="sendRequestUnblockAccount">
                                <input type="hidden" name="id" value="${account.id}">
                                <button class="btn btn-primary w-100 rounded-0" type="submit">
                                    <fmt:message key="accounts.account-unblock"/>
                                </button>
                            </form>
                        </c:when>
                        <c:otherwise>
                            <form class="col-12 m-0 text-center p-0" method="post">
                                <input type="hidden" name="command" value="blockAccount">
                                <input type="hidden" name="id" value="${account.id}">
                                <button class="btn btn-primary w-100 rounded-0" type="submit">
                                    <fmt:message key="accounts.account-block"/>
                                </button>
                            </form>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </c:forEach>

        <filetag:pagination elementsNum="${requestScope.elementsNum}" elementsOnPage="3"/>
    </div>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>
<%@include file="/WEB-INF/jspf/bootstrapScript.jspf" %>
</body>
</html>