<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>

<html>

<%@include file="/WEB-INF/jspf/head.jspf" %>
<link rel="stylesheet" href="style/accounts.css">

<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>

<div class="container d-flex h-75">
    <div class="col-3">
        <div class="row title">
            <h3 class="my-2 text-center">
                <fmt:message key="payments.menu-title"/>
            </h3>
        </div>
        <div class="card container">
            <div class="row align-items-center border-bottom border-dark">
                <div class="col-5 p-0 dropdown text-center border-right">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                       role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <fmt:message key="payments.menu-sorted-by"/>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="?sorting=id&order=${requestScope.order}">
                            <fmt:message key="payments.menu-sorting-number"/>
                        </a></li>
                        <li><a class="dropdown-item" href="?sorting=date&order=${requestScope.order}">
                            <fmt:message key="payments.menu-sorting-date"/>
                        </a></li>
                    </ul>
                </div>
                <div class="col-2 p-0 text-center border-right">
                    <c:choose>
                        <c:when test="${requestScope.order == 'asc'}">
                            <a class="nav-link" href="?sorting=${requestScope.sorting}&order=desc">
                                &dArr;
                            </a>
                        </c:when>
                        <c:otherwise>
                            <a class="nav-link" href="?sorting=${requestScope.sorting}&order=asc">
                                &uArr;
                            </a>
                        </c:otherwise>
                    </c:choose>
                </div>
                <h5 class="col-5 text-center m-0 p-0">
                    ${requestScope.sorting == "id" ? "number" : requestScope.sorting}
                </h5>
            </div>
            <div class="row align-items-center">
                <form class="col-12 m-0 text-center p-0" action="payments/create">
                    <button class="btn btn-primary w-100 rounded-0" type="submit">
                        <fmt:message key="payments.menu-create-payment"/>
                    </button>
                </form>
                <c:if test="${param.success}">
                    <div class="col-12 m-0 p-0 text-center align-items-center">
                        <h5>
                            <fmt:message key="payments.menu-create-payment-success"/>
                        </h5>
                    </div>
                </c:if>
            </div>
            <c:if test="${not empty requestScope.errors}">
                <div class="row alert alert-danger">
                    <c:forEach items="${requestScope.errors}" var="error">
                        <strong><fmt:message key="${error}"/></strong><br>
                    </c:forEach>
                </div>
            </c:if>
        </div>
    </div>

    <div class="col-sm"></div>

    <div class="col-8 justify-content-center">
        <h3 class="my-2 text-center">
            <fmt:message key="payments.title"/>
        </h3>
        <c:forEach items="${requestScope.resource}" var="payment">
            <div class="card container mb-4 border-2 ${payment.statusId == 1 ? "border-warning" : ""}">
                <div class="row border-bottom border-dark">
                    <div class="col-4 p-0 border-right">
                        <h5 class="text-center border-bottom border-dark">
                            <fmt:message key="payments.payment-number"/>
                        </h5>
                        <p class="m-0 text-center">${payment.id}</p>
                    </div>
                    <div class="col-4 p-0 border-right">
                        <h5 class="text-center border-bottom border-dark">
                            <fmt:message key="payments.from-account-number"/>
                        </h5>
                        <p class="m-0 text-center">${payment.fromAccountId}</p>
                    </div>
                    <div class="col-4 p-0">
                        <h5 class="text-center border-bottom border-dark">
                            <fmt:message key="payments.to-account-number"/>
                        </h5>
                        <p class="m-0 text-center">${payment.toAccountId}</p>
                    </div>
                </div>
                <div class="row border-bottom border-dark">
                    <div class="col-4 p-0 border-right">
                        <h5 class="text-center border-bottom border-dark">
                            <fmt:message key="payments.payment-value"/>
                        </h5>
                        <p class="text-center m-0">${payment.value}</p>
                    </div>
                    <div class="col-4 p-0 border-right">
                        <h5 class="text-center border-bottom border-dark">
                            <fmt:message key="payments.payment-status"/>
                        </h5>
                        <p class="text-center m-0">
                            ${payment.statusDescription}
                        </p>
                    </div>
                    <div class="col-4 p-0">
                        <h5 class="text-center border-bottom border-dark">
                            <c:choose>
                                <c:when test="${payment.statusId == 1}">
                                    <fmt:message key="payments.payment-preparing-date"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="payments.payment-send-date"/>
                                </c:otherwise>
                            </c:choose>
                        </h5>
                        <p class="text-center m-0">
                                <fmt:formatDate value="${payment.lastUpdateDate}" pattern="dd/MM/yyyy"/>
                        </p>
                    </div>
                </div>
                <c:choose>
                    <c:when test="${payment.statusId == 1}">
                        <div class="row">
                            <form class="col-12 m-0 text-center p-0" method="post">
                                <input type="hidden" name="command" value="confirmPayment">
                                <input type="hidden" name="id" value="${payment.id}">
                                <button class="btn btn-primary w-100 rounded-0" type="submit">
                                    <fmt:message key="payments.confirm-payment"/>
                                </button>
                            </form>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="row">
                            <form class="col-12 m-0 text-center p-0" method="post">
                                <input type="hidden" name="command" value="generatePdf">
                                <input type="hidden" name="id" value="${payment.id}">
                                <button class="btn btn-primary w-100 rounded-0" type="submit">
                                    <fmt:message key="payments.generate-pdf"/>
                                </button>
                            </form>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </c:forEach>

        <filetag:pagination elementsNum="${requestScope.elementsNum}" elementsOnPage="3" />

    </div>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>
<%@include file="/WEB-INF/jspf/bootstrapScript.jspf" %>
</body>
</html>