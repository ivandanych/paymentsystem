<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>

<html>

<%@include file="/WEB-INF/jspf/head.jspf" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/login.css">

<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%---------------------------------------------------%>
<c:if test="${not empty sessionScope.customer}">
    <c:redirect url="/profile"/>
</c:if>
<%---------------------------------------------------%>
<div class="container d-flex h-75">
    <div class="form-signin row justify-content-center align-self-center border border-2">
        <form class="container" method="post">
            <h1 class="h3 mb-3 font-weight-normal">
                <fmt:message key="login.title"/>
            </h1>
            <c:if test="${param.success.equals('true')}">
            <div class="alert alert-info">
                <strong>
                    <fmt:message key="login.success-registration"/>
                </strong>
            </div>
            </c:if>
            <label for="inputLogin" class="sr-only">
                <fmt:message key="login.login"/>
            </label>
            <input type="text" id="inputLogin" class="form-control" placeholder="<fmt:message key="login.login"/>"
                   name="login" pattern=".{4,}" title="<fmt:message key="login.login-alert"/>"
                   value="${param.login}" required autofocus>
            <label for="inputPassword" class="sr-only">
                <fmt:message key="login.password"/>
            </label>
            <input type="password" id="inputPassword" class="form-control" placeholder="<fmt:message key="login.password"/>"
                   pattern=".{6,}" title="<fmt:message key="login.password-alert"/>" name="password" required>
            <a class="p-2" href="registration">
                <fmt:message key="login.registration"/>
            </a> <br>

            <c:if test="${not empty requestScope.errors}">
                <div class="alert alert-danger">
                    <c:forEach items="${requestScope.errors}" var="error">
                        <strong><fmt:message key="${error}"/></strong><br>
                    </c:forEach>
                </div>
            </c:if>

            <input type="hidden" name="command" value="login"/>
            <button class="btn btn-lg btn-primary btn-block sing-in" type="submit">
                <fmt:message key="login.button"/>
            </button>
        </form>
    </div>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>
<%@include file="/WEB-INF/jspf/bootstrapScript.jspf" %>
</body>
</html>