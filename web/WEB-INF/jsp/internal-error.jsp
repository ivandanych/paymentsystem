<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>

<html>

<%@include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>

<div class="container-md d-flex h-75 justify-content-center">
    <div class="row align-self-center">
        <div class="card p-0">
            <div class="card-header">
                <h4 class="m-0 text-center"><fmt:message key="internal.error.title"/></h4>
            </div>
            <div class="card-body row align-items-center">
                <div class="col-4">
                    <img alt="Error"
                         src="img/errorIcon1.png">
                </div>
                <div class="col-8">
                    <h5 class="text-center">
                        <fmt:message key="internal.error.description"/>
                    </h5>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>
<%@include file="/WEB-INF/jspf/bootstrapScript.jspf" %>
</body>
</html>
