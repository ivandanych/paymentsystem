<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>

<html>

<%@include file="/WEB-INF/jspf/head.jspf" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/adminCustomers.css">

<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>

<div class="container d-flex h-75">
    <div class="col-3">
        <div class="row title">
            <h3 class="my-2 text-center">
                <fmt:message key="admin.accounts.menu-title"/>
            </h3>
        </div>
        <div class="card container">
            <div class="row align-items-center border-bottom border-dark">
                <div class="col-5 p-0 dropdown text-center border-right">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                       role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <fmt:message key="admin.accounts.menu-sorted-by"/>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="?sorting=id&order=${requestScope.order}">
                            <fmt:message key="admin.accounts.menu-sorting-number"/>
                        </a></li>
                        <li><a class="dropdown-item" href="?sorting=balance&order=${requestScope.order}">
                            <fmt:message key="admin.accounts.menu-sorting-balance"/>
                        </a></li>
                    </ul>
                </div>
                <div class="col-2 p-0 text-center border-right">
                    <c:choose>
                        <c:when test="${requestScope.order == 'asc'}">
                            <a class="nav-link" href="?sorting=${requestScope.sorting}&order=desc">
                                &dArr;
                            </a>
                        </c:when>
                        <c:otherwise>
                            <a class="nav-link" href="?sorting=${requestScope.sorting}&order=asc">
                                &uArr;
                            </a>
                        </c:otherwise>
                    </c:choose>
                </div>
                <h5 class="col-5 text-center m-0 p-0">
                    ${requestScope.sorting}
                </h5>
            </div>
        </div>
    </div>

    <div class="col-sm"></div>

    <div class="col-8 justify-content-center">
        <h3 class="my-2 text-center">
            <fmt:message key="admin.accounts.title"/>
        </h3>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.accounts.account-number"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.accounts.account-name"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.accounts.account-balance"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.accounts.account-credit-card"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.accounts.account-customer-name"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.accounts.account-customer-surname"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.accounts.account-blocking"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.accounts.account-date"/>
                        </h5>
                    </th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${requestScope.resource}" var="account">
                <tr>
                    <th scope="row">
                        <p class="text-center m-0">${account.id}</p>
                    </th>
                    <th>
                        <p class="text-center m-0">${account.name}</p>
                    </th>
                    <th>
                        <p class="text-center m-0">${account.balance}</p>
                    </th>
                    <th>
                        <p class="text-center m-0">
                            <mytag:formatCreditCard creditCard="${account.creditCardNumber}" separator="-"/>
                        </p>
                    </th>
                    <th>
                        <p class="text-center m-0">${account.customerName}</p>
                    </th>
                    <th>
                        <p class="text-center m-0">${account.customerSurname}</p>
                    </th>
                    <th class="p-0 m-0 align-items-center">
                        <form class="text-center m-0 h-100" method="post">
                            <input type="hidden" name="command" value="blockUnblockAccount">
                            <input type="hidden" name="id" value="${account.id}">
                            <button class="btn btn-primary w-100 h-100 rounded-0"
                                    type="submit">
                                <c:choose>
                                    <c:when test="${account.blocked}">
                                        <fmt:message key="admin.accounts.account-blocking-unblock" />
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="admin.accounts.account-blocking-block" />
                                    </c:otherwise>
                                </c:choose>
                            </button>
                        </form>
                    </th>
                    <th>
                        <p class="text-center m-0">
                            <fmt:formatDate value="${account.createDate}" pattern="dd/MM/yyyy"/>
                        </p>
                    </th>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <filetag:pagination elementsNum="${requestScope.elementsNum}" elementsOnPage="20"/>
    </div>

    <%@include file="/WEB-INF/jspf/footer.jspf" %>
    <%@include file="/WEB-INF/jspf/bootstrapScript.jspf" %>
</body>
</html>