<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>

<html>

<%@include file="/WEB-INF/jspf/head.jspf" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/adminCustomers.css">

<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>

<div class="container d-flex h-75">
    <div class="col-3">
        <div class="row title">
            <h3 class="my-2 text-center">
                <fmt:message key="admin.requests.menu-title"/>
            </h3>
        </div>
        <div class="card container">
            <div class="row align-items-center border-bottom border-dark">
                <div class="col-5 p-0 dropdown text-center border-right">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                       role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <fmt:message key="admin.requests.menu-sorted-by"/>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="?sorting=date&order=${requestScope.order}">
                            <fmt:message key="admin.requests.menu-sorting-date"/>
                        </a></li>
                        <li><a class="dropdown-item" href="?sorting=accountNumber&order=${requestScope.order}">
                            <fmt:message key="admin.requests.menu-sorting-account-id"/>
                        </a></li>
                    </ul>
                </div>
                <div class="col-2 p-0 text-center border-right">
                    <c:choose>
                        <c:when test="${requestScope.order == 'asc'}">
                            <a class="nav-link" href="?sorting=${requestScope.sorting}&order=desc">
                                &dArr;
                            </a>
                        </c:when>
                        <c:otherwise>
                            <a class="nav-link" href="?sorting=${requestScope.sorting}&order=asc">
                                &uArr;
                            </a>
                        </c:otherwise>
                    </c:choose>
                </div>
                <h5 class="col-5 text-center m-0 p-0">
                    ${requestScope.sorting}
                </h5>
            </div>
        </div>
    </div>

    <div class="col-sm"></div>

    <div class="col-8 justify-content-center">
        <h3 class="my-2 text-center">
            <fmt:message key="admin.requests.title"/>
        </h3>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.requests.request-account-number"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.requests.request-checking"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.requests.request-creation-date-creation"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.requests.request-customer-date-checking"/>
                        </h5>
                    </th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${requestScope.resource}" var="unblockRequests">
                <tr>
                    <th scope="row">
                        <p class="text-center m-0">${unblockRequests.accountId}</p>
                    </th>
                    <th class="p-0 m-0">
                        <c:choose>
                            <c:when test="${unblockRequests.checked}">
                                <p class="text-center m-0">
                                    <fmt:message key="admin.requests.request-checking-checked"/>
                                </p>
                            </c:when>
                            <c:otherwise>
                                <form class="text-center p-0 m-0 h-100" method="post">
                                    <input type="hidden" name="command" value="blockUnblockAccount">
                                    <input type="hidden" name="id" value="${unblockRequests.accountId}">
                                    <button class="btn btn-primary w-100 h-100 rounded-0"
                                            type="submit">
                                        <fmt:message key="admin.requests.account-blocking-unblock" />
                                    </button>
                                </form>
                            </c:otherwise>
                        </c:choose>
                    </th>
                    <th>
                        <p class="text-center m-0">
                            <fmt:formatDate value="${unblockRequests.createDate}" pattern="dd/MM/yyyy"/>
                        </p>
                    </th>
                    <th>
                        <p class="text-center m-0">
                            <c:choose>
                                <c:when test="${unblockRequests.checked}">
                                    <fmt:formatDate value="${unblockRequests.lastUpdate}" pattern="dd/MM/yyyy"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="admin.requests.request-checking-not-checked"/>
                                </c:otherwise>
                            </c:choose>
                        </p>
                    </th>



                </tr>
            </c:forEach>
            </tbody>
        </table>
        <filetag:pagination elementsNum="${requestScope.elementsNum}" elementsOnPage="20"/>
    </div>

    <%@include file="/WEB-INF/jspf/footer.jspf" %>
    <%@include file="/WEB-INF/jspf/bootstrapScript.jspf" %>
</body>
</html>