<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>

<html>

<%@include file="/WEB-INF/jspf/head.jspf" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/adminCustomers.css">

<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>

<div class="container d-flex h-75">
    <div class="col-3">
        <div class="row title">
            <h3 class="my-2 text-center">
                <fmt:message key="admin.customers.menu-title"/>
            </h3>
        </div>
        <div class="card container">
            <div class="row align-items-center border-bottom border-dark">
                <div class="col-5 p-0 dropdown text-center border-right">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                       role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <fmt:message key="admin.customers.menu-sorted-by"/>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="?sorting=surname&order=${requestScope.order}">
                            <fmt:message key="admin.customers.menu-sorting-surname"/>
                        </a></li>
                        <li><a class="dropdown-item" href="?sorting=blocking&order=${requestScope.order}">
                            <fmt:message key="admin.customers.menu-sorting-blocking"/>
                        </a></li>
                    </ul>
                </div>
                <div class="col-2 p-0 text-center border-right">
                    <c:choose>
                        <c:when test="${requestScope.order == 'asc'}">
                            <a class="nav-link" href="?sorting=${requestScope.sorting}&order=desc">
                                &dArr;
                            </a>
                        </c:when>
                        <c:otherwise>
                            <a class="nav-link" href="?sorting=${requestScope.sorting}&order=asc">
                                &uArr;
                            </a>
                        </c:otherwise>
                    </c:choose>
                </div>
                <h5 class="col-5 text-center m-0 p-0">
                    ${requestScope.sorting}
                </h5>
            </div>
        </div>
    </div>

    <div class="col-sm"></div>

    <div class="col-8 justify-content-center">
        <h3 class="my-2 text-center">
            <fmt:message key="admin.customers.title"/>
        </h3>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.customers.customer-name"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.customers.customer-surname"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.customers.customer-email"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.customers.customer-role"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.customers.customer-blocking"/>
                        </h5>
                    </th>
                    <th scope="col">
                        <h5 class="text-center">
                            <fmt:message key="admin.customers.customer-date"/>
                        </h5>
                    </th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${requestScope.resource}" var="customer">
                <tr>
                    <th scope="row">
                        <p class="text-center m-0">${customer.name}</p>
                    </th>
                    <th>
                        <p class="text-center m-0">${customer.surname}</p>
                    </th>
                    <th>
                        <p class="text-center m-0">${customer.email}</p>
                    </th>
                    <th>
                        <p class="text-center m-0">
                            <c:choose>
                                <c:when test="${customer.roleId == 2}">
                                    <fmt:message key="admin.customers.customer-role-admin"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="admin.customers.customer-role-customer"/>
                                </c:otherwise>
                            </c:choose>
                        </p>
                    </th>
                    <th class="p-0">
                        <form class="text-center m-0" method="post">
                            <input type="hidden" name="command" value="blockUnblockCustomer">
                            <input type="hidden" name="id" value="${customer.id}">
                            <button class="btn btn-primary w-100 rounded-0 ${customer.roleId == 2 ? "disabled" : ""}"
                                    type="submit">
                                <c:choose>
                                    <c:when test="${customer.blocked}">
                                        <fmt:message key="admin.customers.customer-blocking-unblock" />
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="admin.customers.customer-blocking-block" />
                                    </c:otherwise>
                                </c:choose>
                            </button>
                        </form>
                    </th>
                    <th>
                        <p class="text-center m-0">
                            <fmt:formatDate value="${customer.createDate}" pattern="dd/MM/yyyy"/>
                        </p>
                    </th>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <filetag:pagination elementsNum="${requestScope.elementsNum}" elementsOnPage="20"/>
    </div>

    <%@include file="/WEB-INF/jspf/footer.jspf" %>
    <%@include file="/WEB-INF/jspf/bootstrapScript.jspf" %>
</body>
</html>