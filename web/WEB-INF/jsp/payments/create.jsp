<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>

<html>

<%@include file="/WEB-INF/jspf/head.jspf" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/create-payment.css">
<script src="${pageContext.request.contextPath}/javascript/paymentCreate.js"></script>

<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>


<div class="container d-flex h-75 justify-content-center">
    <div class="row align-self-center">
        <div class="note">
            <p>
                <fmt:message key="payment.create.title"/>
            </p>
        </div>
        <c:if test="${param.success}">
            <div class="col-12 m-0 p-0 text-center alert alert-success">
                <h5>
                    <fmt:message key="payment.create.success"/>
                </h5>
            </div>
        </c:if>
        <form class="form-content container" method="post">
            <div class="row">
                <div class="col-6 mb-2">
                    <label for="inputFrom" class="sr-only">
                        <fmt:message key="payment.create.number-from"/>
                    </label>
                    <div>
                        <select id="inputFrom" class="form-select" name="fromAccountId">
                            <c:forEach items="${requestScope.accounts}" var="account">
                                <option value="${account.id}" ${param.accountNum == account.id ? "selected" : ""}>
                                    ${account.id}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-6 mb-2">
                    <label for="inputTo" class="sr-only">
                        <fmt:message key="payment.create.number-to"/>
                    </label>
                    <div>
                        <input id="inputTo" type="text" class="form-control" name="toAccountId" value=""
                               placeholder="<fmt:message key="payment.create.number-to"/>" required/>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-6 mb-2">
                    <label for="inputValue" class="sr-only">
                        <fmt:message key="payment.create.value"/>
                    </label>
                    <div class="mb-2">
                        <input id="inputValue" type="number" class="form-control" name="value" min="0" step="0.01"
                               placeholder="<fmt:message key="payment.create.value"/>" required/>
                    </div>
                </div>
                <div class="col-6 mb-2">
                    <label for="inputStatus" class="sr-only">
                        <fmt:message key="payment.create.status"/>
                    </label>
                    <div class="mb-2">
                        <select id="inputStatus" class="form-select" name="status">
                            <option selected value="prepare" onclick="">
                                <fmt:message key="payment.create.prepare"/>
                            </option>
                            <option value="send">
                                <fmt:message key="payment.create.send"/>
                            </option>
                        </select>
                    </div>
                </div>
            </div>

            <c:if test="${not empty requestScope.errors}">
                <div class="alert alert-danger">
                    <c:forEach items="${requestScope.errors}" var="error">
                        <strong><fmt:message key="${error}"/></strong><br>
                    </c:forEach>
                </div>
            </c:if>

            <input type="hidden" name="command" value="createPayment"/>
            <button type="submit" id="submitButton" class="w-100 btn btn-lg btn-primary btn-block">
                <fmt:message key="payment.create.button"/>
            </button>
        </form>
    </div>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>
<%@include file="/WEB-INF/jspf/bootstrapScript.jspf" %>
</body>
</html>