<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>

<html>

<%@include file="/WEB-INF/jspf/head.jspf" %>
<link rel="stylesheet" href="style/registration.css">

<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<%---------------------------------------------------%>
<c:if test="${not empty sessionScope.customer}">
    <c:redirect url="/profile"/>
</c:if>
<%---------------------------------------------------%>
<div class="container d-flex h-75 justify-content-center">
    <div class="row align-self-center">
        <div class="note">
            <p>
                <fmt:message key="registration.title"/>
            </p>
        </div>

        <form class="form-content" method="post"
              oninput='passwordConfirm.setCustomValidity(passwordConfirm.value !== password.value
                      ? <fmt:message key="registration.password_match_alert"/> : "")'>
            <div class="row">
                <div class="col-md-6 mb-2">
                    <label for="inputName" class="sr-only">
                        <fmt:message key="registration.name"/>
                    </label>
                    <div class="mb-2">
                        <input id="inputName" type="text" class="form-control" name="name" value="${param.name}"
                               placeholder="<fmt:message key="registration.name"/>" required/>
                    </div>
                    <label for="inputLogin" class="sr-only">
                        <fmt:message key="registration.login"/>
                    </label>
                    <div class="mb-2">
                        <input id="inputLogin" type="text" class="form-control" name="login" value="${param.login}"
                               placeholder="<fmt:message key="registration.login"/>" required
                               pattern=".{4,}" title="<fmt:message key="registration.login_alert"/>"/>
                    </div>
                    <label for="inputPassword" class="sr-only">
                        <fmt:message key="registration.password"/>
                    </label>
                    <div class="mb-2">
                        <input id="inputPassword" type="password" class="form-control" name='password'
                               placeholder="<fmt:message key="registration.password"/>" required
                               pattern=".{6,}" title="<fmt:message key="registration.password_alert"/>"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="inputSurname" class="sr-only">
                        <fmt:message key="registration.surname"/>
                    </label>
                    <div class="mb-2">
                        <input id="inputSurname" type="text" class="form-control" name="surname" value="${param.surname}"
                               placeholder="<fmt:message key="registration.surname"/>" required/>
                    </div>
                    <label for="inputEmail" class="sr-only">
                        <fmt:message key="registration.email"/>
                    </label>
                    <div class="mb-2">
                        <input id="inputEmail" type="email" class="form-control" name="email" value="${param.email}"
                               placeholder="<fmt:message key="registration.email"/>" required/>
                    </div>
                    <label for="inputPasswordConfirm" class="sr-only">
                        <fmt:message key="registration.confirm_password"/>
                    </label>
                    <div class="mb-2">
                        <input id="inputPasswordConfirm" type="password" class="form-control" name='passwordConfirm'
                               placeholder="<fmt:message key="registration.confirm_password"/>" value="" required/>
                    </div>
                </div>
            </div>

            <c:if test="${not empty requestScope.errors}">
                <div class="alert alert-danger">
                    <c:forEach items="${requestScope.errors}" var="error">
                        <strong><fmt:message key="${error}"/></strong><br>
                    </c:forEach>
                </div>
            </c:if>

            <input type="hidden" name="command" value="register"/>
            <button type="submit" class="w-100 btn btn-lg btn-primary btn-block">
                <fmt:message key="registration.button"/>
            </button>
        </form>
    </div>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>
<%@include file="/WEB-INF/jspf/bootstrapScript.jspf" %>
</body>
</html>