<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>

<html>

<%@include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>

<div class="container-md d-flex h-75 justify-content-center">
    <div class="row align-items-center">
        <div class="col-5">
            <h1 class="text-center">
                <fmt:message key="main.welcome"/>
            </h1>
        </div>
        <div class="col-5">
            <img src="img/HomeImage-removebg-preview.png" alt="Online payments">
        </div>
    </div>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>
<%@include file="/WEB-INF/jspf/bootstrapScript.jspf" %>
</body>
</html>
