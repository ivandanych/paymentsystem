<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>

<html>

<%@include file="/WEB-INF/jspf/head.jspf" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/create-payment.css">

<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>


<div class="container d-flex h-75 justify-content-center">
    <div class="row align-self-center">
        <div class="note">
            <p>
                <fmt:message key="receipt.create.title"/>
            </p>
        </div>
        <c:if test="${param.success}">
            <div class="col-12 m-0 p-0 text-center alert alert-success">
                <h5>
                    <fmt:message key="receipt.create.success"/>
                </h5>
            </div>
        </c:if>
        <form class="form-content container" method="post">
            <div class="row">
                <div class="col-6 mb-2">
                    <label for="inputTo" class="sr-only">
                        <fmt:message key="receipt.create.number-to"/>
                    </label>
                    <div>
                        <select id="inputTo" class="form-select" name="toAccountId">
                            <c:forEach items="${requestScope.accounts}" var="account">
                                <option value="${account.id}" ${param.accountNum == account.id ? "selected" : ""}>
                                    ${account.id}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-6 mb-2">
                    <label for="inputValue" class="sr-only">
                        <fmt:message key="receipt.create.value"/>
                    </label>
                    <div class="mb-2">
                        <input id="inputValue" type="number" class="form-control"
                               name="value" min="0" step="0.01"
                               placeholder="<fmt:message key="receipt.create.value"/>" required/>
                    </div>
                </div>
            </div>

            <c:if test="${not empty requestScope.errors}">
                <div class="alert alert-danger">
                    <c:forEach items="${requestScope.errors}" var="error">
                        <strong><fmt:message key="${error}"/></strong><br>
                    </c:forEach>
                </div>
            </c:if>

            <input type="hidden" name="command" value="createReceipt"/>
            <button type="submit" id="submitButton" class="w-100 btn btn-lg btn-primary btn-block">
                <fmt:message key="receipt.create.button"/>
            </button>
        </form>
    </div>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>
<%@include file="/WEB-INF/jspf/bootstrapScript.jspf" %>
</body>
</html>