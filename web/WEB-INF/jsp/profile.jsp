<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@include file="/WEB-INF/jspf/choseLocale.jspf" %>

<html>

<%@include file="/WEB-INF/jspf/head.jspf" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/profile1.css"/>

<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>

<div class="container d-flex h-75 justify-content-center">
    <div class="row align-self-center">
        <div class="card p-0">
            <div class="card-header">
                <h4 class="m-0"><fmt:message key="profile.title"/></h4>
            </div>
            <div class="card-body row align-items-center">
                <div class="col-4">
                    <img alt="User Pic"
                         src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg"
                         id="profile-image1" class="img-circle img-responsive" width="360" height="360">
                </div>
                <div class="col-8">
                    <div class="container">
                        <h2>${sessionScope.customer.name} ${sessionScope.customer.surname}</h2>
                        <p>an
                            <b>
                            <c:choose>
                                <c:when test="${sessionScope.customer.roleId == 2}">
                                    <fmt:message key="profile.admin-role"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="profile.customer-role"/>
                                </c:otherwise>
                            </c:choose>
                            </b>
                        </p>
                    </div>
                    <hr>
                    <ul class="container details">
                        <li>
                            <p>
                                <img src="${pageContext.request.contextPath}/img/userIcon.png"
                                     width="25" height="25" alt="Login:"/>
                                ${sessionScope.customer.login}
                            </p>
                        </li>
                        <li>
                            <p>
                                <img src="${pageContext.request.contextPath}/img/mailIcon.png"
                                     width="25" height="25" alt="Mail:"/>
                                ${sessionScope.customer.email}
                            </p>
                        </li>
                    </ul>
                    <hr>
                    <div class="text-center title">
                        <fmt:message key="profile.date-of-joining"/>
                        <fmt:formatDate value="${sessionScope.customer.createDate}" pattern="dd MMMM yyyy"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>
<%@include file="/WEB-INF/jspf/bootstrapScript.jspf" %>
</body>
</html>
