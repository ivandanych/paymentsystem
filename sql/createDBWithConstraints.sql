-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema paypool
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `paypool` ;

-- -----------------------------------------------------
-- Schema paypool
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `paypool` DEFAULT CHARACTER SET utf8 ;
USE `paypool` ;

-- -----------------------------------------------------
-- Table `paypool`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `paypool`.`role` ;

CREATE TABLE IF NOT EXISTS `paypool`.`role` (
  `id` INT NOT NULL,
  `name` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `paypool`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `paypool`.`customer` ;

CREATE TABLE IF NOT EXISTS `paypool`.`customer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(30) NOT NULL,
  `password` VARCHAR(64) NOT NULL,
  `email` VARCHAR(60) NOT NULL,
  `name` VARCHAR(30) NOT NULL,
  `surname` VARCHAR(30) NOT NULL,
  `is_blocked` TINYINT NOT NULL DEFAULT 0,
  `role_id` INT NOT NULL DEFAULT 1,
  `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC) VISIBLE,
  INDEX `fk_customer_role1_idx` (`role_id` ASC) VISIBLE,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  CONSTRAINT `fk_customer_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `paypool`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT login_has_min_length_4 CHECK (LENGTH(login) >= 4),
  CONSTRAINT email_has_min_length_4 CHECK (LENGTH(email) >= 4)
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `paypool`.`account`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `paypool`.`account` ;

CREATE TABLE IF NOT EXISTS `paypool`.`account` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(40) NOT NULL,
  `balance` DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT 0,
  `credit_card_number` VARCHAR(16) NOT NULL,
  `is_blocked` TINYINT NOT NULL DEFAULT 0,
  `customer_id` INT NOT NULL,
  `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `customer_id`),
  INDEX `fk_account_customer1_idx` (`customer_id` ASC) VISIBLE,
  UNIQUE INDEX `credit_card_number_UNIQUE` (`credit_card_number` ASC) VISIBLE,
  CONSTRAINT `fk_account_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `paypool`.`customer` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT credit_card_has_correct_length_16 CHECK (LENGTH(credit_card_number) = 16),
  CONSTRAINT account_name_has_min_length_4 CHECK (LENGTH(name) >=  4)
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `paypool`.`status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `paypool`.`status` ;

CREATE TABLE IF NOT EXISTS `paypool`.`status` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `paypool`.`payment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `paypool`.`payment` ;

CREATE TABLE IF NOT EXISTS `paypool`.`payment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `from_account_id` INT NOT NULL,
  `to_account_id` INT NOT NULL,
  `value` DECIMAL(10,2) UNSIGNED NOT NULL,
  `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_id` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `fk_payment_status1_idx` (`status_id` ASC) VISIBLE,
  INDEX `fk_from_account_idx` (`from_account_id` ASC) VISIBLE,
  INDEX `fk_to_account_idx` (`to_account_id` ASC) VISIBLE,
  CONSTRAINT `fk_payment_status1`
    FOREIGN KEY (`status_id`)
    REFERENCES `paypool`.`status` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_from_account`
    FOREIGN KEY (`from_account_id`)
    REFERENCES `paypool`.`account` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_to_account`
    FOREIGN KEY (`to_account_id`)
    REFERENCES `paypool`.`account` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `paypool`.`receipt`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `paypool`.`receipt` ;

CREATE TABLE IF NOT EXISTS `paypool`.`receipt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `to_account_id` INT NOT NULL,
  `value` DECIMAL(10,2) UNSIGNED NOT NULL,
  `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_account_from_receipt_idx` (`to_account_id` ASC) VISIBLE,
  CONSTRAINT `fk_account_to_receipt`
    FOREIGN KEY (`to_account_id`)
    REFERENCES `paypool`.`account` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `paypool`.`withdraw`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `paypool`.`withdraw` ;

CREATE TABLE IF NOT EXISTS `paypool`.`withdraw` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `from_account_id` INT NOT NULL,
  `value` DECIMAL(10,2) UNSIGNED NOT NULL,
  `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_account_from_receipt_idx` (`from_account_id` ASC) VISIBLE,
  CONSTRAINT `fk_account_to_withdaw`
    FOREIGN KEY (`from_account_id`)
    REFERENCES `paypool`.`account` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE = InnoDB;

-- Table `paypool`.`unblock_request`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `paypool`.`unblock_request` ;

CREATE TABLE IF NOT EXISTS `paypool`.`unblock_request` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `account_id` INT NOT NULL,
  `is_checked` TINYINT NOT NULL DEFAULT 0,
  `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `account_id`),
  CONSTRAINT `fk_unblock_request_account`
    FOREIGN KEY (`id`)
    REFERENCES `paypool`.`account` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `paypool`.`language`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `paypool`.`language` ;

CREATE TABLE IF NOT EXISTS `paypool`.`language` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `value` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `paypool`.`status_details`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `paypool`.`status_details` ;

CREATE TABLE IF NOT EXISTS `paypool`.`status_details` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `status_id` INT NOT NULL,
  `language_id` INT NOT NULL,
  `description` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id`, `status_id`),
  INDEX `fk_role_to_status_details_idx` (`status_id` ASC) VISIBLE,
  INDEX `fk_language_to_status_idx` (`language_id` ASC) VISIBLE,
  CONSTRAINT `fk_status_to_status_details`
    FOREIGN KEY (`status_id`)
    REFERENCES `paypool`.`status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_language_to_status`
    FOREIGN KEY (`language_id`)
    REFERENCES `paypool`.`language` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `paypool`;

DELIMITER $$

#USE `paypool`$$
#DROP TRIGGER IF EXISTS `paypool`.`payment_BEFORE_INSERT` $$
#USE `paypool`$$
#CREATE DEFINER = CURRENT_USER TRIGGER `paypool`.`payment_BEFORE_INSERT` BEFORE INSERT ON `payment` FOR EACH ROW
#BEGIN
#	IF NEW.status_id = 2 THEN
#		#START transaction;   
#			UPDATE account
#			SET balance = balance + NEW.value
#			WHERE id = NEW.to_account_id;
#			UPDATE account
#			SET balance = balance - NEW.value
#			WHERE id = NEW.from_account_id;
#        #COMMIT;
#	END IF;
#END$$

USE `paypool`$$
DROP TRIGGER IF EXISTS `paypool`.`account_BEFORE_UPDATE` $$
USE `paypool`$$
CREATE DEFINER = CURRENT_USER TRIGGER `paypool`.`account_BEFORE_UPDATE` BEFORE UPDATE ON `account` FOR EACH ROW
BEGIN
IF NEW.is_blocked = 0 THEN
		UPDATE unblock_request
		SET is_checked = 1
		WHERE account_id = OLD.id;
	END IF;
END$$

#USE `paypool`$$
#DROP TRIGGER IF EXISTS `paypool`.`payment_BEFORE_UPDATE` $$
#USE `paypool`$$
#CREATE DEFINER = CURRENT_USER TRIGGER `paypool`.`payment_BEFORE_UPDATE` BEFORE UPDATE ON `payment` FOR EACH ROW
#BEGIN
#	IF NEW.status_id = 2 THEN
#		#START TRANSACTION;
#			UPDATE account
#			SET balance = balance + OLD.value
#			WHERE id = OLD.to_account_id;
#			UPDATE account
#			SET balance = balance - OLD.value
#			WHERE id = OLD.from_account_id;
#        #COMMIT;
#	END IF;
#END$$


USE `paypool`$$
DROP TRIGGER IF EXISTS `paypool`.`receipt_AFTER_INSERT` $$
USE `paypool`$$
CREATE DEFINER = CURRENT_USER TRIGGER `paypool`.`receipt_AFTER_INSERT` AFTER INSERT ON `receipt` FOR EACH ROW
BEGIN
 UPDATE account
 SET balance = balance + NEW.value
 WHERE account.id = NEW.to_account_id;
END$$


USE `paypool`$$
DROP TRIGGER IF EXISTS `paypool`.`withdraw_BEFORE_INSERT` $$
USE `paypool`$$
CREATE DEFINER = CURRENT_USER TRIGGER `paypool`.`withdraw_BEFORE_INSERT` BEFORE INSERT ON `withdraw` FOR EACH ROW
BEGIN
 UPDATE account
 SET balance = balance - NEW.value
 WHERE account.id = NEW.from_account_id;
END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `paypool`.`role`
-- -----------------------------------------------------
START TRANSACTION;
USE `paypool`;
INSERT INTO `paypool`.`role` (`id`, `name`) VALUES (1, 'CUSTOMER');
INSERT INTO `paypool`.`role` (`id`, `name`) VALUES (2, 'ADMIN');

COMMIT;


-- -----------------------------------------------------
-- Data for table `paypool`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `paypool`;
INSERT INTO `paypool`.`customer` (`id`, `login`, `password`, `email`, `name`, `surname`, `is_blocked`, `role_id`, `create_date`, `last_update`) VALUES (1, 'ivandanych', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'dynychivan@email.com', 'Ivan', 'Danych', 0, 2, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`customer` (`id`, `login`, `password`, `email`, `name`, `surname`, `is_blocked`, `role_id`, `create_date`, `last_update`) VALUES (2, 'davinchi', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'vinchi@gmail.com', 'Leonardo', 'Vinchi', 0, 1, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`customer` (`id`, `login`, `password`, `email`, `name`, `surname`, `is_blocked`, `role_id`, `create_date`, `last_update`) VALUES (3, 'vlad1', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'vlad1@gmail.com', 'Vlad', 'First', 0, 1, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`customer` (`id`, `login`, `password`, `email`, `name`, `surname`, `is_blocked`, `role_id`, `create_date`, `last_update`) VALUES (4, 'vlad2', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'vlad2@gmail.com', 'Vlad', 'Second', 0, 1, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`customer` (`id`, `login`, `password`, `email`, `name`, `surname`, `is_blocked`, `role_id`, `create_date`, `last_update`) VALUES (5, 'vlad3', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'vlad3@gmail.com', 'Vlad', 'Thrid', 0, 1, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`customer` (`id`, `login`, `password`, `email`, `name`, `surname`, `is_blocked`, `role_id`, `create_date`, `last_update`) VALUES (6, 'vlad4', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'vlad4@gmail.com', 'Vlad', 'Fourth', 0, 1, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`customer` (`id`, `login`, `password`, `email`, `name`, `surname`, `is_blocked`, `role_id`, `create_date`, `last_update`) VALUES (7, 'vlad5', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'vlad5@gmail.com', 'Vlad', 'Fiveth', 0, 1, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`customer` (`id`, `login`, `password`, `email`, `name`, `surname`, `is_blocked`, `role_id`, `create_date`, `last_update`) VALUES (8, 'vlad6', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'vlad6@gmail.com', 'Vlad', 'Sixth', 0, 1, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`customer` (`id`, `login`, `password`, `email`, `name`, `surname`, `is_blocked`, `role_id`, `create_date`, `last_update`) VALUES (9, 'vlad7', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'vlad7@gmail.com', 'Vlad', 'Seventh', 0, 1, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`customer` (`id`, `login`, `password`, `email`, `name`, `surname`, `is_blocked`, `role_id`, `create_date`, `last_update`) VALUES (10, 'vlad8', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'vlad8@gmail.com', 'Vlad', 'Eighth', 0, 1, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`customer` (`id`, `login`, `password`, `email`, `name`, `surname`, `is_blocked`, `role_id`, `create_date`, `last_update`) VALUES (11, 'vlad9', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'vlad9@gmail.com', 'Vlad', 'Nineth', 0, 1, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`customer` (`id`, `login`, `password`, `email`, `name`, `surname`, `is_blocked`, `role_id`, `create_date`, `last_update`) VALUES (12, 'vlad10', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'vlad10@gmail.com', 'Vlad', 'Chukran', 0, 1, '2021-01-01', '2021-01-01');

COMMIT;


-- -----------------------------------------------------
-- Data for table `paypool`.`account`
-- -----------------------------------------------------
START TRANSACTION;
USE `paypool`;
INSERT INTO `paypool`.`account` (`id`, `name`, `balance`, `credit_card_number`, `is_blocked`, `customer_id`, `create_date`, `last_update`) VALUES (1, 'For food', 300, '1234567890123456', 0, 1, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`account` (`id`, `name`, `balance`, `credit_card_number`, `is_blocked`, `customer_id`, `create_date`, `last_update`) VALUES (2, 'For something', 340, '4444444444444444', 0, 2, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`account` (`id`, `name`, `balance`, `credit_card_number`, `is_blocked`, `customer_id`, `create_date`, `last_update`) VALUES (3, 'For car', 240, '2222222222222222', 0, 1, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`account` (`id`, `name`, `balance`, `credit_card_number`, `is_blocked`, `customer_id`, `create_date`, `last_update`) VALUES (4, 'Third', 100, '5555555555555555', 0, 1, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`account` (`id`, `name`, `balance`, `credit_card_number`, `is_blocked`, `customer_id`, `create_date`, `last_update`) VALUES (5, 'Forth', 100, '6666666666666666', 0, 1, '2021-01-01', '2021-01-01');
INSERT INTO `paypool`.`account` (`id`, `name`, `balance`, `credit_card_number`, `is_blocked`, `customer_id`, `create_date`, `last_update`) VALUES (6, 'Fifth', 200, '7777777777777777', 0, 1, '2021-01-01', '2021-01-01');

COMMIT;


-- -----------------------------------------------------
-- Data for table `paypool`.`status`
-- -----------------------------------------------------
START TRANSACTION;
USE `paypool`;
INSERT INTO `paypool`.`status` (`id`, `name`) VALUES (1, 'PREPARED');
INSERT INTO `paypool`.`status` (`id`, `name`) VALUES (2, 'DONE');

COMMIT;

-- -----------------------------------------------------
-- Data for table `paypool`.`receipt`
-- -----------------------------------------------------
START TRANSACTION;
USE `paypool`;
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (1, 1, 250, '2021-01-01');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (2, 1, 100, '2021-01-02');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (3, 2, 400, '2021-01-03');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (4, 2, 100, '2021-01-04');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (5, 3, 50, '2021-01-05');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (6, 1, 50, '2021-01-05');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (7, 2, 50, '2021-01-06');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (8, 3, 50, '2021-01-12');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (9, 3, 50, '2021-01-12');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (10, 3, 50, '2021-01-12');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (11, 3, 50, '2021-01-12');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (12, 3, 50, '2021-01-12');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (13, 3, 50, '2021-01-12');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (14, 3, 50, '2021-01-12');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (15, 3, 50, '2021-01-12');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (16, 3, 50, '2021-01-12');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (17, 3, 50, '2021-01-12');
INSERT INTO `paypool`.`receipt` (`id`, `to_account_id`, `value`, `create_date`) VALUES (18, 3, 50, '2021-01-12');

COMMIT;


-- -----------------------------------------------------
-- Data for table `paypool`.`payment`
-- -----------------------------------------------------
START TRANSACTION;
USE `paypool`;
INSERT INTO `paypool`.`payment` (`id`, `from_account_id`, `to_account_id`, `value`, `create_date`, `last_update`, `status_id`) VALUES (1, 1, 2, 20, '2021-01-01', '2021-01-01', 2);
INSERT INTO `paypool`.`payment` (`id`, `from_account_id`, `to_account_id`, `value`, `create_date`, `last_update`, `status_id`) VALUES (2, 1, 3, 10, '2021-01-01', '2021-01-02', 2);
INSERT INTO `paypool`.`payment` (`id`, `from_account_id`, `to_account_id`, `value`, `create_date`, `last_update`, `status_id`) VALUES (3, 3, 2, 15, '2021-01-01', '2021-01-03', 2);
INSERT INTO `paypool`.`payment` (`id`, `from_account_id`, `to_account_id`, `value`, `create_date`, `last_update`, `status_id`) VALUES (4, 2, 3, 10, '2021-01-01', '2021-01-04', 2);
INSERT INTO `paypool`.`payment` (`id`, `from_account_id`, `to_account_id`, `value`, `create_date`, `last_update`, `status_id`) VALUES (5, 3, 1, 5, '2021-01-01', '2021-01-05', 2);
INSERT INTO `paypool`.`payment` (`id`, `from_account_id`, `to_account_id`, `value`, `create_date`, `last_update`, `status_id`) VALUES (6, 1, 3, 10, '2021-01-01', '2021-01-06', 1);

COMMIT;

-- -----------------------------------------------------
-- Data for table `paypool`.`withdraw`
-- -----------------------------------------------------
START TRANSACTION;
USE `paypool`;
INSERT INTO `paypool`.`withdraw` (`id`, `from_account_id`, `value`, `create_date`) VALUES (1, 1, 10, '2021-01-01');
INSERT INTO `paypool`.`withdraw` (`id`, `from_account_id`, `value`, `create_date`) VALUES (2, 1, 5, '2021-01-01');
INSERT INTO `paypool`.`withdraw` (`id`, `from_account_id`, `value`, `create_date`) VALUES (3, 2, 10, '2021-01-02');
INSERT INTO `paypool`.`withdraw` (`id`, `from_account_id`, `value`, `create_date`) VALUES (4, 3, 5, '2021-01-03');
INSERT INTO `paypool`.`withdraw` (`id`, `from_account_id`, `value`, `create_date`) VALUES (5, 1, 15, '2021-01-04');
INSERT INTO `paypool`.`withdraw` (`id`, `from_account_id`, `value`, `create_date`) VALUES (6, 2, 25, '2021-01-05');
INSERT INTO `paypool`.`withdraw` (`id`, `from_account_id`, `value`, `create_date`) VALUES (7, 3, 35, '2021-01-06');

COMMIT;

-- -----------------------------------------------------
-- Data for table `paypool`.`language`
-- -----------------------------------------------------
START TRANSACTION;
USE `paypool`;
INSERT INTO `paypool`.`language` (`id`, `value`) VALUES (1, 'en');
INSERT INTO `paypool`.`language` (`id`, `value`) VALUES (2, 'uk');

COMMIT;


-- -----------------------------------------------------
-- Data for table `paypool`.`status_details`
-- -----------------------------------------------------
START TRANSACTION;
USE `paypool`;
INSERT INTO `paypool`.`status_details` (`id`, `status_id`, `language_id`, `description`) VALUES (1, 1, 1, 'Created but not sent');
INSERT INTO `paypool`.`status_details` (`id`, `status_id`, `language_id`, `description`) VALUES (2, 1, 2, 'Створений але не відправлений');
INSERT INTO `paypool`.`status_details` (`id`, `status_id`, `language_id`, `description`) VALUES (3, 2, 1, 'Created and sent');
INSERT INTO `paypool`.`status_details` (`id`, `status_id`, `language_id`, `description`) VALUES (4, 2, 2, 'Створений та надісланий');

COMMIT;